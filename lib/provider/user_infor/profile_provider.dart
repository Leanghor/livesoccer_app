import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter_demo3/models/profile_model.dart';
import 'package:flutter_demo3/repositories/authentication/profile_repo.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileControllerProvider extends ChangeNotifier {
  bool? isAgree = false;
  File? imageFile;
  var image = ProfileRepository();
  List<ProfileModel> imgProfile = [];

  void imageProfile() async {
    await image.fetchUserProfile().then((value) {
      if (value != null) {
        imgProfile = value;
      }
    });
    notifyListeners();
  }

  void agreeWithPolicy() {
    isAgree = true;
    notifyListeners();
  }

  void disagreeWithPolicy() {
    isAgree = false;
    notifyListeners();
  }

  Future<dynamic> clearImageProfile() async {
    // imageProfile.$ = null;
    notifyListeners();
  }

  Future<dynamic> pickFromGallery() async {
    XFile? xFile = await ImagePicker()
        .pickImage(source: ImageSource.gallery, imageQuality: 100);
    if (xFile != null) {
      PickedFile? image = PickedFile(xFile.path);
      imageFile = File(image.path);
      await imageFile?.writeAsBytes(await image.readAsBytes());
    } else {
      return null;
    }
    notifyListeners();
  }

  Future<dynamic> pickFromCamera() async {
    XFile? xFile = await ImagePicker()
        .pickImage(source: ImageSource.camera, imageQuality: 100);
    if (xFile != null) {
      PickedFile? image = PickedFile(xFile.path);
      imageFile = File(image.path);
      await imageFile?.writeAsBytes(await image.readAsBytes());
    } else {
      return null;
    }
    notifyListeners();
  }

  void saveImageFilePath(String path) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('imageFilePath', path);
    notifyListeners();
  }

  Future<String?> getImageFilePath() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    notifyListeners();
    return prefs.getString('imageFilePath');
  }

  Future<String?> loadImageFilePath() async {
    String? path = await getImageFilePath();
    if (path != null) {
      imageFile = File(path);
    }
    notifyListeners();
    return null;
  }
}
