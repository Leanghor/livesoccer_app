import 'package:flutter/cupertino.dart';
import 'package:flutter_demo3/models/league_one_model.dart';

import '../repositories/league_one_repo.dart';

class LeagueOneProvider extends ChangeNotifier{
  List<LeagueOneModel> pushInto = [];
  bool waiting = true;
  final _fetch = OneRepository();

  lOneData() async {
    waiting = true;

    pushInto = (await _fetch.callApi());
    waiting = false;
    notifyListeners();
  }

}