import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo3/base_url/globle_uri.dart';
import 'package:flutter_demo3/models/user_model.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../globle/globle_key.dart';
import '../repositories/login_repo.dart';

class LoginProviderController extends ChangeNotifier {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController messageController = TextEditingController();

  final newName = TextEditingController();
  String? selectedValue;
  dynamic value;
  final newBio = TextEditingController();

  var repository = LoginRepository();
  bool isDeleted = false;
  dynamic deleted;
  int? status;
  int? mesStatus;
  String? message;
  String? hasDelete;
  UserModel? userModel;
  File? profileImage;
  String? coverImage;
  String? userRole;
  String? url = baseUrl;

  Future<bool> authLogin() async {
    try {
      var value = await repository.userLogin(
        email: emailController.text,
        password: passwordController.text,
      );
      if (value != null) {
        storeToken.$ = value["access_token"];
        await storeToken.save();
        return true;
      }
    } catch (e) {
      if (kDebugMode) {
        print("Login error: $e");
      }
    }
    notifyListeners();
    return false;
  }


  void updateUser(UserModel? u, {bool isUpdate = false}) {
    if (u != null) {
      userModel = u;
    }

    if (isUpdate) {
      notifyListeners();
    }
  }

  Future<void> authLogOut() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var token = sharedPreferences.getString('access_token');
    if (token != null) {
      await sharedPreferences.remove('access_token');
    } else {
      if (kDebugMode) {
        print('Have no token for remove ///');
      }
    }
  }

  sendMessageFromUser({required String content}) async {
    await repository.submitMessage(notification: messageController.text).then((v) {
      if (v != null) {
        status = v['status'];
        message = v['message'];
        debugPrint("message $message");
      }
    });
  }

  getUserInformation() async {
    await repository.fetchUser().then((value){
      if(value != null){
        userModel = value;
      }
    });
    notifyListeners();
  }

  Future<dynamic> getImageFromCamera() async {
    try{
      final pickedFile = await ImagePicker().pickImage(source: ImageSource.camera);

      if (pickedFile != null) {
        profileImage = File(pickedFile.path);
        updatePhoto(profileImage);
      }
      notifyListeners();
    }catch(_){}
  }

  Future<dynamic> getImageFromGallery() async {
    try{
      final pickedFile = await ImagePicker().pickImage(source: ImageSource.gallery);
      if (pickedFile != null) {
        profileImage = File(pickedFile.path);
        updatePhoto(profileImage);
        debugPrint("%% $profileImage");
      }
      notifyListeners();
    }catch(_){}
  }

  deleteMessageData(String id) async {
    deleted = repository.deleteDataRepo(id).then((value){
      if(value != null) {
        mesStatus = repository.status;
        print("=======KK $hasDelete");
      }
    });
    if (kDebugMode) {
      print("error: $deleted");
    }
    notifyListeners();
  }

  void clearMessage() async {
    messageController.clear();
    notifyListeners();
  }

  void updateNameUser(String id) async {
    userModel!.name = newName.text;
    userModel!.role = selectedValue.toString();
    notifyListeners();

    try {
      value = await repository.updateNameANDEmail(id, role: selectedValue.toString(), name: newName.text);
      userModel = UserModel.fromJson(value);
      updateUser(userModel, isUpdate: true);
      debugPrint("==========-- $selectedValue");
      notifyListeners();
    } catch (error) {
      debugPrint("An error occurred: $error");
    }
  }

  void updatePhoto(File? image) async {
    await repository.updateProfile(userModel!.id.toString(), imageFile: image!.path).then((v) => {
      if(v != null){
        updateUser(v, isUpdate: true)
      }
    });
    notifyListeners();
  }

  void removeUserImage(String? id, {bool listenUpdate = false}) async {
    try{
      await repository.removeUserImage(id: id.toString()).then((ph) => {
        if(ph != null){
          message = ph['message'],
          updateUser(ph, isUpdate: true)
        }
      });
      if(listenUpdate){
        notifyListeners();
      }
      debugPrint("=========*** $message");
    }catch(_){}finally{
      notifyListeners();
    }
  }
}


