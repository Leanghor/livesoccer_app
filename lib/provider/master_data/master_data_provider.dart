import 'package:flutter/cupertino.dart';
import 'package:flutter_demo3/models/master_premier_model.dart';
import 'package:flutter_demo3/repositories/master_data/master_data_repo.dart';

class MasterDataProvider extends ChangeNotifier{
  List<MasterModel> mld = [];
  var getMLD = MasterDataRepo();
  bool? isLoading = false;

  void fetchMLD({required String nameClub}) async {
    isLoading = true;
    mld = getMLD.masterBizModelList(nameClub: nameClub);

    isLoading = false;
    notifyListeners();
  }
}