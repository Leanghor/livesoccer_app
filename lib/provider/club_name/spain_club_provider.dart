import 'package:flutter/cupertino.dart';
import '../../models/club_name/spain_club.dart';
import '../../repositories/clubs/spain_club_repo.dart';

class SapinClubProvider extends ChangeNotifier {
  List<ClubSapinModel> provideClub = [];
  bool isLoading = true;
  final _club = ClubSpainRepo();

  getClub() async {
    provideClub = await _club.nameClub();
    isLoading = false;
    notifyListeners();
  }
}
