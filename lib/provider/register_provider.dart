import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../base_url/globle_uri.dart';

class SignUpProvider extends ChangeNotifier{
  Future<dynamic> registerRepo({
    required String username,
    required String password,
    required String passwordConfirm,
    required String email}) async {
    var request = http.MultipartRequest(
        'POST', Uri.parse('$baseUri/api/auth/register'));
    request.fields.addAll({
      'name': username,
      'password': password,
      'password_confirmation': passwordConfirm,
      'email': email
    });
    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var data = (await response.stream.bytesToString());
      var json = jsonDecode(data);
      if (kDebugMode) {
        print('gfgggggg $json');
      }
      return json;
    }
    else {
      print(response.reasonPhrase);
    }
  }
}

