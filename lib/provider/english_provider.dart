import 'package:flutter/cupertino.dart';
import '../models/english_model.dart';
import '../repositories/english_repo.dart';

class PremierLeagueProvider extends ChangeNotifier {
  List<PremierLeagueModel> tempE = [];
  bool isLoading = true;
  String message = '';
  final _e = PremierLeagueRepository();

  finalGetDataE() async {
    isLoading = true;

    tempE = await _e.efetch();
    isLoading = false;
    notifyListeners();
  }
}