import 'package:flutter/cupertino.dart';
import 'package:flutter_demo3/repositories/seriA_repo.dart';
import '../models/seriA_model.dart';

class SeriAProvider extends ChangeNotifier{
  List<SeriAModel> seri = [];
  bool isLoading = true;
  final _fetchFormRepo = SeriARepository();

  functionGetDataLast() async {
    isLoading = true;

    seri = await _fetchFormRepo.seriA();
    isLoading = false;
    notifyListeners();
  }
}