import 'package:flutter/cupertino.dart';
import '../models/besdesliga_model.dart';
import '../repositories/busdesliga_repo.dart';

class BundesligaProvider extends ChangeNotifier{
  List<BundesligaModel> lastData = [];

  bool isLoading = true;
  final _service = BusdesligaRepository();

  bData() async {
    isLoading = true;

    lastData = await _service.bunData();
    isLoading = false;

    notifyListeners();
  }


}