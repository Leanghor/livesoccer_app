import 'package:flutter/cupertino.dart';
import 'package:flutter_demo3/models/laliga_model.dart';
import 'package:flutter_demo3/repositories/laliga_repo.dart';

class LaligaProvider extends ChangeNotifier {
  List<LaligaModel> push = [];

  bool isLoading = true;
  final _tempLaliga = LaligaRepository();
  pushDataFrom() async {
    isLoading = true;
    push = (await _tempLaliga.fetchDataFromLaliga());
    isLoading = false;
    notifyListeners();
  }
}