import 'package:flutter/cupertino.dart';
import 'package:flutter_demo3/models/ufa_model.dart';
import '../repositories/ufa_repo.dart';

class EuFaProvider extends ChangeNotifier {
  List<EuFaModel> data = [];

  bool isLoading = true;
  //String message = '';
  final euFaRepository = EuFaRepository();
  getPostData() async {
    isLoading = true;
    data = (await euFaRepository.getData());

    isLoading = false;
    notifyListeners();
  }
}