// ignore_for_file: unnecessary_this

class ProfileModel {
  String? id;
  String? userId;
  String? image;
  String? role;
  String? createdAt;
  String? updatedAt;

  ProfileModel(
      {this.id,
      this.userId,
      this.image,
      this.role,
      this.createdAt,
      this.updatedAt});

  ProfileModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    userId = json['user_id'].toString();
    image = json['image'].toString();
    role = json['role'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id.toString();
    data['user_id'] = this.userId.toString();
    data['image'] = this.image.toString();
    data['role'] = this.role.toString();
    data['created_at'] = this.createdAt.toString();
    data['updated_at'] = this.updatedAt.toString();
    return data;
  }
}
