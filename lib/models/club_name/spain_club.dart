class ClubSapinModel {
  String? id;
  String? name;
  String? logo;
  String? stadium;
  String? location;
  String? startDate;
  String? laliga;
  String? campaign;
  String? copaDelrey;
  String? createdAt;
  String? updatedAt;

  ClubSapinModel(
      {this.id,
        this.name,
        this.logo,
        this.stadium,
        this.location,
        this.startDate,
        this.laliga,
        this.campaign,
        this.copaDelrey,
        this.createdAt,
        this.updatedAt});

  ClubSapinModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    name = json['name'];
    logo = json['logo'];
    stadium = json['stadium'];
    location = json['location'];
    startDate = json['start_date'];
    laliga = json['laliga'].toString();
    campaign = json['campaign'].toString();
    copaDelrey = json['copa_delrey'].toString();
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id.toString();
    data['name'] = name;
    data['logo'] = logo;
    data['stadium'] = stadium;
    data['location'] = location;
    data['start_date'] = startDate;
    data['laliga'] = laliga.toString();
    data['campaign'] = campaign.toString();
    data['copa_delrey'] = copaDelrey.toString();
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
