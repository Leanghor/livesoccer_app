class MasterModel {
  String? id;
  String? nameClub;
  String? session;
  String? image;
  List<Lost>? lost;
  List<Won>? won;
  List<Drawn>? drawn;
  List<ClubInformation>? clubInformation;

  MasterModel(
      {this.id,
        this.nameClub,
        this.session,
        this.image,
        this.lost,
        this.won,
        this.drawn,
        this.clubInformation});

  MasterModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameClub = json['name_club'];
    session = json['session'];
    image = json['image'];
    if (json['lost'] != null) {
      lost = <Lost>[];
      json['lost'].forEach((v) {
        lost!.add(Lost.fromJson(v));
      });
    }
    if (json['won'] != null) {
      won = <Won>[];
      json['won'].forEach((v) {
        won!.add(Won.fromJson(v));
      });
    }
    if (json['drawn'] != null) {
      drawn = <Drawn>[];
      json['drawn'].forEach((v) {
        drawn!.add(Drawn.fromJson(v));
      });
    }
    if (json['club_information'] != null) {
      clubInformation = <ClubInformation>[];
      json['club_information'].forEach((v) {
        clubInformation!.add(ClubInformation.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name_club'] = nameClub;
    data['session'] = session;
    data['image'] = image;
    if (lost != null) {
      data['lost'] = lost!.map((v) => v.toJson()).toList();
    }
    if (won != null) {
      data['won'] = won!.map((v) => v.toJson()).toList();
    }
    if (drawn != null) {
      data['drawn'] = drawn!.map((v) => v.toJson()).toList();
    }
    if (clubInformation != null) {
      data['club_information'] =
          clubInformation!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Drawn {
  String? key;

  Drawn({this.key});

  Drawn.fromJson(Map<String, dynamic> json) {
    key = json['key'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['key'] = key;
    return data;
  }
}

class Won {
  String? key;

  Won({this.key});

  Won.fromJson(Map<String, dynamic> json) {
    key = json['key'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['key'] = key;
    return data;
  }
}

class Lost {
  String? key;

  Lost({this.key});

  Lost.fromJson(Map<String, dynamic> json) {
    key = json['key'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['key'] = key;
    return data;
  }
}

class ClubInformation {
  String? logo;
  String? cover;
  String? mp;
  String? won;
  String? drawn;
  String? lost;
  String? gf;
  String? ga;
  String? gd;
  String? pt;

  ClubInformation(
      {this.logo,
        this.cover,
        this.mp,
        this.won,
        this.drawn,
        this.lost,
        this.gf,
        this.ga,
        this.gd,
        this.pt});

  ClubInformation.fromJson(Map<String, dynamic> json) {
    logo = json['logo'];
    cover = json['cover'];
    mp = json['mp'];
    won = json['won'];
    drawn = json['drawn'];
    lost = json['lost'];
    gf = json['gf'];
    ga = json['ga'];
    gd = json['gd'];
    pt = json['pt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['logo'] = logo;
    data['cover'] = cover;
    data['mp'] = mp;
    data['won'] = won;
    data['drawn'] = drawn;
    data['lost'] = lost;
    data['gf'] = gf;
    data['ga'] = ga;
    data['gd'] = gd;
    data['pt'] = pt;
    return data;
  }
}
