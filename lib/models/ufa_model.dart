class EuFaModel {
  String? id;
  String? homeNameClub;
  String? imageHomeClub;
  String? gdHomeClub;
  String? enemyNameClub;
  String? imageEnemyClub;
  String? gdEnemyClub;
  String? createdAt;
  String? updatedAt;

  EuFaModel(
      {this.id,
        this.homeNameClub,
        this.imageHomeClub,
        this.gdHomeClub,
        this.enemyNameClub,
        this.imageEnemyClub,
        this.gdEnemyClub,
        this.createdAt,
        this.updatedAt});

  EuFaModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    homeNameClub = json['homeNameClub'].toString();
    imageHomeClub = json['imageHomeClub'].toString();
    gdHomeClub = json['gdHomeClub'].toString();
    enemyNameClub = json['enemyNameClub'].toString();
    imageEnemyClub = json['imageEnemyClub'].toString();
    gdEnemyClub = json['gdEnemyClub'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id.toString();
    data['homeNameClub'] = homeNameClub.toString();
    data['imageHomeClub'] = imageHomeClub.toString();
    data['gdHomeClub'] = gdHomeClub.toString().toString();
    data['enemyNameClub'] = enemyNameClub.toString();
    data['imageEnemyClub'] = imageEnemyClub.toString();
    data['gdEnemyClub'] = gdEnemyClub.toString();
    data['created_at'] = createdAt.toString();
    data['updated_at'] = updatedAt.toString();
    return data;
  }
}
