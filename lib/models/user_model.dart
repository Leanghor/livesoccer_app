class UserModel {
  int? id;
  String? name;
  String? email;
  Null? emailVerifiedAt;
  String? profileImage;
  String? coverImage;
  String? role;
  String? createdAt;
  String? updatedAt;
  List<Messages>? messages;

  UserModel(
      {this.id,
        this.name,
        this.email,
        this.emailVerifiedAt,
        this.profileImage,
        this.coverImage,
        this.role,
        this.createdAt,
        this.updatedAt,
        this.messages});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    profileImage = json['profile_image'];
    coverImage = json['cover_image'];
    role = json['role'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['messages'] != null) {
      messages = <Messages>[];
      json['messages'].forEach((v) {
        messages!.add(new Messages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['email_verified_at'] = emailVerifiedAt;
    data['profile_image'] = profileImage;
    data['cover_image'] = coverImage;
    data['role'] = role;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    if (messages != null) {
      data['messages'] = messages!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Messages {
  int? id;
  int? userId;
  String? notification;
  String? createdAt;
  String? updatedAt;

  Messages(
      {this.id,
        this.userId,
        this.notification,
        this.createdAt,
        this.updatedAt});

  Messages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    notification = json['notification'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['user_id'] = userId;
    data['notification'] = notification;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
