class LaligaModel {
  String? id;
  String? nameHomeClub;
  String? imgHomeClub;
  String? gdHomeClub;
  String? nameEnemyClub;
  String? imgEnemyClub;
  String? gdEnemyClub;
  String? createdAt;
  String? updatedAt;

  LaligaModel({this.id,
    this.nameHomeClub,
    this.imgHomeClub,
    this.gdHomeClub,
    this.nameEnemyClub,
    this.imgEnemyClub,
    this.gdEnemyClub,
    this.createdAt,
    this.updatedAt});

  LaligaModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    nameHomeClub = json['name_home_club'];
    imgHomeClub = json['img_home_club'];
    gdHomeClub = json['gd_home_club'];
    nameEnemyClub = json['name_enemy_club'];
    imgEnemyClub = json['img_enemy_club'];
    gdEnemyClub = json['gd_enemy_club'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id.toString();
    data['name_home_club'] = nameHomeClub;
    data['img_home_club'] = imgHomeClub;
    data['gd_home_club'] = gdHomeClub;
    data['name_enemy_club'] = nameEnemyClub;
    data['img_enemy_club'] = imgEnemyClub;
    data['gd_enemy_club'] = gdEnemyClub;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
