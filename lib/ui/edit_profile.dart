import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo3/provider/login_provider.dart';
import 'package:provider/provider.dart';

class EditProfileScreen extends StatefulWidget {
  const EditProfileScreen({
    super.key,
  });

  @override
  State<EditProfileScreen> createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  final List<String> items = [
    'User',
    'Admin',
    'Maintain',
    'CEO',
  ];

  @override
  Widget build(BuildContext context) {
    return Consumer<LoginProviderController>(
      builder: (context, dataUser, child) {
        return Scaffold(
          appBar: AppBar(
            title: const Text(
              'Edit Profile',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            centerTitle: true,
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 10),
                child: GestureDetector(
                  onTap: () {
                    dataUser.updateNameUser(dataUser.userModel!.id.toString());
                    Navigator.pop(context);
                    setState(() {});
                  },
                  child: const Text(
                    "Done",
                    style: TextStyle(
                        color: Colors.blueAccent,
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        decorationColor: Colors.blueAccent,
                        decoration: TextDecoration.underline),
                  ),
                ),
              )
            ],
          ),
          body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Image.network(
                      dataUser.userModel != null
                        && dataUser.userModel!.profileImage != null
                        && dataUser.userModel!.profileImage!.isNotEmpty
                        ? "${dataUser.url}${dataUser.userModel!.profileImage.toString()}"
                        : "https://i.pinimg.com/736x/a8/57/00/a85700f3c614f6313750b9d8196c08f5.jpg",
                      width: 130,
                      height: 130,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                userInfo(
                  controller: dataUser.newName,
                  hintText: (dataUser.userModel != null && dataUser.userModel!.name != null)
                    ? dataUser.userModel!.name.toString()
                    : "Enter new name",
                  label: "New name",
                ),

                const SizedBox(
                  height: 20,
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    color: Colors.white, // Background color
                    borderRadius: BorderRadius.circular(14), // Border radius
                    border: Border.all(color: Colors.grey.shade300), // Border color and width
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      hint: Text(dataUser.userModel!.role != null ? dataUser.userModel!.role.toString() : "Select",
                        style: const TextStyle(color: Colors.grey), // Hint text style
                      ),
                      value: dataUser.selectedValue,
                      icon: const Icon(Icons.arrow_drop_down, color: Colors.grey),
                      isExpanded: true,
                      items: items.map((String item) => DropdownMenuItem<String>(
                        value: item,
                        child: Text(
                          item,
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      )).toList(),
                      onChanged: (String? value) {
                        setState(() {
                          dataUser.selectedValue = value;
                        });
                        debugPrint("===== ${dataUser.selectedValue}");
                      },
                      dropdownColor:
                          Colors.white, // Dropdown menu background color
                    ),
                  ),
                )
              ],
            ),
          )
        );
      },
    );
  }

  Widget userInfo(
      {required TextEditingController controller,
      required String hintText,
      required String label}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 16,
        ),
        Text(
          label,
          style: const TextStyle(fontSize: 14, color: Colors.black),
          textAlign: TextAlign.start,
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(8),
          ),
          child: TextFormField(
            controller: controller,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.all(12),
              hintText: hintText,
              border: InputBorder.none,
            ),
            style: const TextStyle(fontSize: 16),
            enabled: true, // Enable editing
          ),
        ),
      ],
    );
  }
}
