import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo3/ui/dynamic_screen.dart';
import 'package:provider/provider.dart';
import '../provider/club_name/spain_club_provider.dart';

class LeagueApp extends StatefulWidget {
  const LeagueApp({super.key});

  @override
  State<LeagueApp> createState() => _LeagueAppState();
}

class _LeagueAppState extends State<LeagueApp> {
  String? sesion = "2023/2024";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<SapinClubProvider>(context, listen: false).getClub();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<SapinClubProvider>(
      builder: (context, provider, child) {
        return RefreshIndicator(
          onRefresh: () {
            return Future<void>.delayed(const Duration(seconds: 1));
          },
          child: Scaffold(
              body: CustomScrollView(
                slivers: [
                  SliverAppBar(
                    automaticallyImplyLeading: false,
                    flexibleSpace: FlexibleSpaceBar(
                      background: Container(
                        decoration: const BoxDecoration(
                          gradient: LinearGradient(
                            colors: [Colors.blue, Colors.green],
                            // Set your gradient colors
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                      ),
                    ),
                    centerTitle: false,
                    title: const Text('Worlds League',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18)
                    ),
                    actions: [
                      InkWell(
                        onTap: () {
                          // Dialogs.bottomMaterialDialog(
                          //   context: context,
                          //   actions: [
                          //     Column(
                          //       children: [
                          //         Center(
                          //           child: SvgPicture.asset(
                          //             'assets/icons/line.svg',
                          //             height: 30,
                          //             fit: BoxFit.cover,
                          //           ),
                          //         ),
                          //         const SizedBox(height: 5),
                          //         ListView.builder(
                          //           physics: const NeverScrollableScrollPhysics(),
                          //           shrinkWrap: true,
                          //           padding: EdgeInsets.zero,
                          //           itemCount: provider.league.length,
                          //           itemBuilder: (context, index) {
                          //             return InkWell(
                          //               onTap: () {
                          //                 Navigator.push(context, MaterialPageRoute(
                          //                   builder: (context) =>
                          //                       DynamicLeagueScreen(
                          //                     title: '',
                          //                       widgetClass: '',
                          //                       img: '',
                          //                     ),),
                          //                   );
                          //                 },
                          //                 child: Column(children: [
                          //                     Padding(
                          //                       padding: const EdgeInsets.symmetric(
                          //                         vertical: 5,
                          //                       ),
                          //                       child: Row(
                          //                         children: [
                          //                           Container(
                          //                             width: 40,
                          //                             height: 40,
                          //                             padding:
                          //                                 const EdgeInsets.all(10),
                          //                             decoration: BoxDecoration(
                          //                               gradient:
                          //                                   const LinearGradient(
                          //                                 begin: Alignment.topLeft,
                          //                                 end:
                          //                                     Alignment.bottomRight,
                          //                                 colors: [
                          //                                   Colors.blue,
                          //                                   Colors.green,
                          //                                 ],
                          //                               ),
                          //                               borderRadius:
                          //                                   BorderRadius.circular(
                          //                                       5),
                          //                             ),
                          //                             child: SvgPicture.asset(
                          //                               'assets/icons/player.svg',
                          //                               fit: BoxFit.contain,
                          //                               color: Colors.orangeAccent,
                          //                             ),
                          //                           ),
                          //                           const SizedBox(width: 10),
                          //                           Text(
                          //                             "",
                          //                             style: const TextStyle(
                          //                               fontWeight: FontWeight.bold,
                          //                               fontSize: 18,
                          //                             ),
                          //                           ),
                          //                         ],
                          //                       ),
                          //                     ),
                          //                     const Divider(
                          //                       height: 5,
                          //                       color: Colors.black,
                          //                     ),
                          //                   ],
                          //                 ),
                          //               );
                          //             },
                          //           ),
                          //         ],
                          //       )
                          //   ]
                          // );
                        },
                        borderRadius: BorderRadius.circular(50),
                        child: const SizedBox(
                          width: 50,
                          height: 50,
                          child: Icon(
                            Icons.keyboard_arrow_down_outlined,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      )
                    ],
                  ),
                  SliverToBoxAdapter(
                    child: CachedNetworkImage(
                      imageUrl:
                          'https://i.pinimg.com/564x/b3/74/52/b37452d2808e8d5ae0e42c39f09ef838.jpg',
                      imageBuilder: (context, imageProvider) => Container(
                        margin: const EdgeInsets.all(10),
                        height: 200,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      placeholder: (context, url) => const SizedBox(
                          width: 30,
                          height: 30,
                          child: Center(
                            child: CircularProgressIndicator(
                              color: Colors.blue,
                            ),
                          )),
                      errorWidget: (context, url, error) => const Icon(Icons.error),
                    ),
                  ),
                  SliverToBoxAdapter(
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        padding: const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                        decoration: BoxDecoration(
                          border: const Border(
                              left: BorderSide(
                                color: Colors.blue,
                                width: 5.0,
                              ),
                          ),
                            color: Colors.grey.withOpacity(0.3),
                            borderRadius: BorderRadius.circular(15)),
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text("Premier League",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.underline
                                    )),
                                Text("Seasion: $sesion",
                                    style: TextStyle(fontSize: 10, color: Colors.black.withOpacity(0.4))),
                                const SizedBox(height: 20,),
                                InkWell(
                                  borderRadius: BorderRadius.circular(5),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                          const DynamicLeagueScreen(
                                            leagueCountry: "premier_league",
                                            title: "Premier League",
                                            imageLogo:
                                                "https://www.premierleague.com/resources/rebrand/v7.134.0/i/elements/pl-main-logo.png",
                                          )
                                      )
                                    );
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                                    child: const Text("More", style: TextStyle(
                                      color: Colors.blueAccent,
                                      decoration: TextDecoration.underline,
                                      decorationColor: Colors.blueAccent,
                                      fontSize: 14,
                                    )),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                CachedNetworkImage(
                                  imageUrl:
                                      'https://www.premierleague.com/resources/rebrand/v7.134.0/i/elements/pl-main-logo.png',
                                  imageBuilder: (context, imageProvider) => Container(
                                    margin: const EdgeInsets.all(10),
                                    height: 60,
                                    width: 80,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      image: DecorationImage(
                                        image: imageProvider,
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ),
                                  placeholder: (context, url) => const SizedBox(
                                    width: 30,
                                    height: 30,
                                    child: Center(
                                      child: CircularProgressIndicator(
                                        color: Colors.blue,
                                      ),
                                    )),
                                  errorWidget: (context, url, error) =>
                                    const Icon(Icons.error),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  SliverToBoxAdapter(
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        padding: const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                        decoration: BoxDecoration(
                          border: const Border(
                              left: BorderSide(
                                color: Colors.blue,
                                width: 5.0,
                              ),
                          ),
                            color: Colors.grey.withOpacity(0.3),
                            borderRadius: BorderRadius.circular(15)),
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text("Ligue1",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline
                                  )
                                ),
                                Text("Seasion: $sesion",
                                    style: TextStyle(fontSize: 10, color: Colors.black.withOpacity(0.4))),
                                const SizedBox(height: 20,),
                                InkWell(
                                  borderRadius: BorderRadius.circular(5),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                          const DynamicLeagueScreen(
                                            leagueCountry: "ligue_one",
                                            title: "Ligue1",
                                            imageLogo:
                                                "https://www.fifplay.com/img/public/ligue-1-logo-transparent.png",
                                          )
                                      )
                                    );
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                                    child: const Text("More", style: TextStyle(
                                      color: Colors.blueAccent,
                                      decoration: TextDecoration.underline,
                                      decorationColor: Colors.blueAccent,
                                      fontSize: 14,
                                    )),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                CachedNetworkImage(
                                  imageUrl:
                                      'https://www.fifplay.com/img/public/ligue-1-logo-transparent.png',
                                  imageBuilder: (context, imageProvider) => Container(
                                    margin: const EdgeInsets.all(10),
                                    height: 60,
                                    width: 80,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      image: DecorationImage(
                                        image: imageProvider,
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ),
                                  placeholder: (context, url) => const SizedBox(
                                      width: 30,
                                      height: 30,
                                      child: Center(
                                        child: CircularProgressIndicator(
                                          color: Colors.blue,
                                        ),
                                      )),
                                  errorWidget: (context, url, error) =>
                                      const Icon(Icons.error),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),

                  SliverToBoxAdapter(
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        padding: const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                        decoration: BoxDecoration(
                          border: const Border(
                              left: BorderSide(
                                color: Colors.blue,
                                width: 5.0,
                              ),
                          ),
                            color: Colors.grey.withOpacity(0.3),
                            borderRadius: BorderRadius.circular(15)),
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text("Laliga",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.underline
                                    )),
                                Text("Seasion: $sesion",
                                    style: TextStyle(fontSize: 10, color: Colors.black.withOpacity(0.4))),
                                const SizedBox(height: 20,),
                                InkWell(
                                  borderRadius: BorderRadius.circular(5),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                           const DynamicLeagueScreen(
                                            leagueCountry: "laliga",
                                            title: "Laliga Santander",
                                            imageLogo: "https://tiermaker.com/images/templates/laliga-2023-24-15867433/158674331687389508.png",
                                          )
                                      )
                                    );
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                                    child: const Text("More", style: TextStyle(
                                      color: Colors.blueAccent,
                                      decoration: TextDecoration.underline,
                                      decorationColor: Colors.blueAccent,
                                      fontSize: 14,
                                    )),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                CachedNetworkImage(
                                  imageUrl:
                                      'https://crystalpng.com/wp-content/uploads/2023/06/La-liga-new-logo-circle-red.png',
                                  imageBuilder: (context, imageProvider) => Container(
                                    margin: const EdgeInsets.all(10),
                                    height: 60,
                                    width: 80,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      image: DecorationImage(
                                        image: imageProvider,
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ),
                                  placeholder: (context, url) => const SizedBox(
                                    width: 30,
                                    height: 30,
                                    child: Center(
                                      child: CircularProgressIndicator(
                                        color: Colors.blue,
                                      ),
                                    )
                                  ),
                                  errorWidget: (context, url, error) =>
                                      const Icon(Icons.error),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),

                  SliverToBoxAdapter(
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                      decoration: BoxDecoration(
                          border: const Border(
                            left: BorderSide(
                              color: Colors.blue,
                              width: 5.0,
                            ),
                          ),
                          color: Colors.grey.withOpacity(0.3),
                          borderRadius: BorderRadius.circular(15)),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Seri A",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.underline
                                  )),
                              Text("Seasion: $sesion",
                                  style: TextStyle(fontSize: 10, color: Colors.black.withOpacity(0.4))),
                              const SizedBox(height: 20,),
                              InkWell(
                                borderRadius: BorderRadius.circular(5),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                          const DynamicLeagueScreen(
                                            leagueCountry: "seri_a",
                                            title: "Italy SeriA",
                                            imageLogo:
                                            "https://www.fifplay.com/img/public/serie-a-logo-transparent.png",
                                          )
                                      )
                                  );
                                },
                                child: Container(
                                  padding: const EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                                  child: const Text("More", style: TextStyle(
                                    color: Colors.blueAccent,
                                    decoration: TextDecoration.underline,
                                    decorationColor: Colors.blueAccent,
                                    fontSize: 14,
                                  )),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              CachedNetworkImage(
                                imageUrl:
                                'https://www.fifplay.com/img/public/serie-a-logo-transparent.png',
                                imageBuilder: (context, imageProvider) => Container(
                                  margin: const EdgeInsets.all(10),
                                  height: 60,
                                  width: 80,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) => const SizedBox(
                                    width: 30,
                                    height: 30,
                                    child: Center(
                                      child: CircularProgressIndicator(
                                        color: Colors.blue,
                                      ),
                                    )),
                                errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),

                  SliverToBoxAdapter(
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                      decoration: BoxDecoration(
                          border: const Border(
                            left: BorderSide(
                              color: Colors.blue,
                              width: 5.0,
                            ),
                          ),
                          color: Colors.grey.withOpacity(0.3),
                          borderRadius: BorderRadius.circular(15)),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Bundesliga",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.underline
                                  )),
                              Text("Seasion: $sesion",
                                  style: TextStyle(fontSize: 10, color: Colors.black.withOpacity(0.4))),
                              const SizedBox(height: 20,),
                              InkWell(
                                borderRadius: BorderRadius.circular(10),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                          const DynamicLeagueScreen(
                                            leagueCountry: "bundesliga",
                                            title: "Bundesliga",
                                            imageLogo:
                                            "https://cdn.freebiesupply.com/logos/large/2x/bundesliga-2-logo-black-and-white.png",
                                          )
                                      )
                                  );
                                },
                                child: Container(
                                  padding: const EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                                  child: const Text("More", style: TextStyle(
                                    color: Colors.blueAccent,
                                    decoration: TextDecoration.underline,
                                    decorationColor: Colors.blueAccent,
                                    fontSize: 14,
                                  )),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              CachedNetworkImage(
                                imageUrl:
                                "https://cdn.freebiesupply.com/logos/large/2x/bundesliga-2-logo-black-and-white.png",
                                imageBuilder: (context, imageProvider) => Container(
                                  margin: const EdgeInsets.all(10),
                                  height: 60,
                                  width: 80,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) => const SizedBox(
                                    width: 30,
                                    height: 30,
                                    child: Center(
                                      child: CircularProgressIndicator(
                                        color: Colors.blue,
                                      ),
                                    )),
                                errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
          )),
        );
      },
    );
  }
}
