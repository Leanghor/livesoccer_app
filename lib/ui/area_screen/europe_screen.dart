import 'package:flutter/material.dart';
import 'main_league/bundesliga.dart';
import 'main_league/laliga.dart';
import 'main_league/league1.dart';
import 'main_league/premier.dart';
import 'main_league/seriA.dart';
import 'main_league/ufa.dart';

class EuropeScreen extends StatefulWidget {
  const EuropeScreen({super.key});

  @override
  State<EuropeScreen> createState() => _EuropeScreenState();
}

class _EuropeScreenState extends State<EuropeScreen> {
  @override
  void initState() {

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 8, right: 8, top: 30),
          child: Row(
            children: [
              Text("Live Match",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
              Text(
                "*",
                style: TextStyle(color: Colors.red, fontSize: 18),
              ),
            ],
          ),
        ),
        UFA(),
        LaligaScreen(),
        LeagueOneScreen(),
        SeriAScreen(),
        PremierLeagueScreen(),
        BundesligaScreen(),

        SizedBox(height: 10,),
      ]
    );
  }
}
