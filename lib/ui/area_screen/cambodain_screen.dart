import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'cambodia/cpl.dart';
import 'cambodia/hsc.dart';
import 'cambodia/soccer.dart';

class CambodiaScreen extends StatefulWidget {
  const CambodiaScreen({super.key});

  @override
  State<CambodiaScreen> createState() => _CambodiaScreenState();
}

class _CambodiaScreenState extends State<CambodiaScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  bool soccer = true;
  bool cpl = false;
  bool hsc = false;
  int currentlyPage = 0;
  var linkPage = [
     const SoccerScreen(),
     CPLScreen(),
     const HSCScreen()
  ];

  void _onTap(int index){
    currentlyPage = index;
    switch(index){
      case 0:
         const SoccerScreen();
         soccer = true;
         cpl = false;
         hsc = false;
        break;
      case 1:
         CPLScreen();
         cpl = true;
         soccer = false;
         hsc = false;
        break;
      case 2:
         const HSCScreen();
         hsc = true;
         soccer = false;
         cpl = false;
      break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: scaffoldKey,
      child: Container(
        margin: const EdgeInsets.only(left: 8, right: 8, top: 16),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  onTap: (){
                    setState(() {
                      _onTap(0);
                    });
                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                      color: Colors.blue.shade900,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue.shade900.withOpacity(0.2),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset('assets/icons/soccer.svg', width: 50, height: 50, fit: BoxFit.contain,),
                        Text('Soccer', style: TextStyle(
                            fontSize: 16,
                            color: soccer ? Colors.green : Colors.white,
                            fontWeight: FontWeight.bold
                        ),),
                        const Text('2023')
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: (){
                    setState(() {
                      _onTap(1);
                    });
                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                      color: Colors.blue.shade900.withOpacity(1),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue.shade900.withOpacity(0.2),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset('assets/icons/soccer (1).svg', width: 50, height: 50, fit: BoxFit.contain,),
                        Text('CPL', style: TextStyle(
                            fontSize: 16,
                            color: cpl ? Colors.red : Colors.white,
                            fontWeight: FontWeight.bold
                        ),),
                        const Text('2023')
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: (){
                    setState(() {
                      _onTap(2);
                    });
                  },
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                      color: Colors.blue.shade900.withOpacity(1),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue.shade900.withOpacity(0.2),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset('assets/icons/soccer (2).svg', width: 50, height: 50, fit: BoxFit.contain,),
                        Text('CL2', style: TextStyle(
                            fontSize: 16,
                            color: hsc ? Colors.orangeAccent : Colors.white,
                            fontWeight: FontWeight.bold
                        ),),
                        const Text('2023')
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20,),
            const Divider(height: 10, color: Colors.black,),
            linkPage.elementAt(currentlyPage),
          ],
        ),
      ),
    );
  }
}


