import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import '../../../provider/league_one_provider.dart';

class LeagueOneScreen extends StatefulWidget {
  const LeagueOneScreen({super.key});

  @override
  State<LeagueOneScreen> createState() => _LeagueOneScreenState();
}

class _LeagueOneScreenState extends State<LeagueOneScreen> {

  @override
  void initState() {
    Provider.of<LeagueOneProvider>(context, listen: false).lOneData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LeagueOneProvider>(builder: (context, lOne, child) {
      return Container(
          margin: const EdgeInsets.only(top: 20, left: 8, right: 8),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.black.withOpacity(lOne.waiting ? 0.0 : 0.1)),
          child: lOne.waiting ? const Center(
              child: SpinKitFadingCircle(
                color: Colors.black,
                size: 35.0,
              )
          ) : Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Image.asset(
                      'assets/images/ligue-removebg-preview.png',
                      width: 100,
                      height: 50,
                      fit: BoxFit.cover,
                    ),
                  ),
                  const Icon(Icons.more_vert)
                ],
              ),
              const Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                    ),
                    child: Text(
                      'Recently Matches.',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                itemCount: lOne.pushInto.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Container(
                    margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                    padding: const EdgeInsets.only(
                        top: 8, left: 10, right: 10, bottom: 8),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white.withOpacity(0.7)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image.network(
                                  lOne.pushInto[index].imgHomeClub ?? '',
                                  fit: BoxFit.contain,
                                  width: 25,
                                  height: 25,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(lOne.pushInto[index].nameHomeClub ?? ''),
                              ],
                            ),
                            Text(lOne.pushInto[index].gdHomeClub ?? ''),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image.network(
                                  lOne.pushInto[index].imgEnemyClub ?? '',
                                  fit: BoxFit.contain,
                                  width: 25,
                                  height: 25,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(lOne.pushInto[index].nameEnemyClub ??
                                    ''),
                              ],
                            ),
                            Text(lOne.pushInto[index].gdEnemyClub ?? ''),
                          ],
                        ),
                      ],
                    ),
                  );
                },
              ),
            ],
          )
      );
    },
    );
  }
}
