import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import '../../../provider/seriA_provider.dart';

class SeriAScreen extends StatefulWidget {
  const SeriAScreen({super.key});

  @override
  State<SeriAScreen> createState() => _SeriAScreenState();
}

class _SeriAScreenState extends State<SeriAScreen> {
  @override
  void initState() {
    Provider.of<SeriAProvider>(context, listen: false).functionGetDataLast();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<SeriAProvider>(builder: (context, provider, child) {
      return Container(
          margin: const EdgeInsets.only(top: 20, left: 8, right: 8),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.black.withOpacity(provider.isLoading ? 0.0 : 0.1)),
          child: provider.isLoading ? const Center(
              child: SpinKitFadingCircle(
                color: Colors.black,
                size: 35.0,
              )
          ) : Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Image.asset(
                      'assets/logoClub/Logo_Serie_A_TIM_2021-removebg-preview.png',
                      width: 50,
                      height: 60,
                      fit: BoxFit.contain,
                    ),
                  ),
                  const Icon(Icons.more_vert)
                ],
              ),
              const Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                    ),
                    child: Text(
                      'Recently Matches.',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                itemCount: provider.seri.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Container(
                    margin: const EdgeInsets.only(
                        left: 20, right: 20, bottom: 10),
                    padding: const EdgeInsets.only(
                        top: 8, left: 10, right: 10, bottom: 8),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white.withOpacity(0.7)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image.network(
                                  provider.seri[index].imgHomeClub ?? '',
                                  fit: BoxFit.contain,
                                  width: 25,
                                  height: 25,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(
                                    provider.seri[index].nameHomeClub ?? ''),
                              ],
                            ),
                            Text(provider.seri[index].gdHomeClub ?? ''),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image.network(
                                  provider.seri[index].imgEnemyClub ?? '',
                                  fit: BoxFit.contain,
                                  width: 25,
                                  height: 25,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(provider
                                    .seri[index].nameEnemyClub ??
                                    ''),
                              ],
                            ),
                            Text(
                                provider.seri[index].gdEnemyClub ?? ''),
                          ],
                        ),
                      ],
                    ),
                  );
                },
              ),
            ],
          ));
    },
    );
  }
}
