import 'package:flutter/material.dart';
import 'package:flutter_demo3/provider/bundesliga_provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class BundesligaScreen extends StatefulWidget {
  const BundesligaScreen({super.key});

  @override
  State<BundesligaScreen> createState() => _BundesligaScreenState();
}

class _BundesligaScreenState extends State<BundesligaScreen> {
  @override
  void initState() {
    Provider.of<BundesligaProvider>(context, listen: false).bData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<BundesligaProvider>( builder: (context, provider, child) {
      return Container(
        margin: const EdgeInsets.only(top: 10, left: 8, right: 8),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.black.withOpacity(provider.isLoading ? 0.0 : 0.1)),
        child: provider.isLoading ? const Center(
            child: SpinKitFadingCircle(
              color: Colors.black,
              size: 35.0,
            )
        ) : Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, top: 10),
                  child: Image.asset(
                    'assets/logoClub/Bundesliga-removebg-preview.png',
                    width: 60,
                    height: 50,
                    fit: BoxFit.contain,
                  ),
                ),
                const Icon(Icons.more_vert)
              ],
            ),
            const Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                  ),
                  child: Text(
                    'Recently Matches.',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            ListView.builder(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              itemCount: provider.lastData.length,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return provider.isLoading
                    ? const Center(
                    child: Text(
                      'Loading...',
                      style: TextStyle(color: Colors.red),
                    ))
                    : Container(
                  margin: const EdgeInsets.only(
                      left: 20, right: 20, bottom: 20),
                  padding: const EdgeInsets.only(
                      top: 8, left: 10, right: 10, bottom: 8),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white.withOpacity(0.7)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Image.network(
                                provider.lastData[index].imgHomeClub ?? '',
                                fit: BoxFit.contain,
                                width: 25,
                                height: 25,
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Text(provider.lastData[index].nameHomeClub ??
                                  ''),
                            ],
                          ),
                          Text(provider.lastData[index].gdHomeClub ?? ''),
                        ],
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Image.network(
                                provider.lastData[index].imgEnemyClub ?? '',
                                fit: BoxFit.contain,
                                width: 25,
                                height: 25,
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Text(provider.lastData[index].nameEnemyClub ??
                                  ''),
                            ],
                          ),
                          Text(provider.lastData[index].gdEnemyClub ?? ''),
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
          ],
        ),
      );
    },
    );
  }
}
