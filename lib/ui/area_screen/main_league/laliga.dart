import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import '../../../provider/laliga_provider.dart';

class LaligaScreen extends StatefulWidget {
  const LaligaScreen({super.key});

  @override
  State<LaligaScreen> createState() => _LaligaScreenState();
}

class _LaligaScreenState extends State<LaligaScreen> {
  @override
  void initState() {
    Provider.of<LaligaProvider>(context, listen: false).pushDataFrom();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LaligaProvider>(builder: (context, laligaProvider, child) {
      return Container(
          margin: const EdgeInsets.only(top: 20, left: 8, right: 8),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.black.withOpacity(laligaProvider.isLoading ? 0.0 : 0.1)),
          child: laligaProvider.isLoading ? const Center(
              child: SpinKitFadingCircle(
                color: Colors.black,
                size: 35.0,
              )
          ) : Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Image.asset(
                      'assets/images/laliga.png',
                      width: 100,
                      height: 50,
                      fit: BoxFit.cover,
                    ),
                  ),
                  const Icon(Icons.more_vert)
                ],
              ),
              const Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                    ),
                    child: Text(
                      'Recently Matches.',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 15,),
              ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                itemCount: laligaProvider.push.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Container(
                    margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                    padding: const EdgeInsets.only(top: 8, left: 10, right: 10, bottom: 8),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white.withOpacity(0.7)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image.network(
                                  laligaProvider.push[index].imgHomeClub ?? '',
                                  fit: BoxFit.contain,
                                  width: 25,
                                  height: 25,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(laligaProvider.push[index].nameHomeClub ??
                                    ''),
                              ],
                            ),
                            Text(laligaProvider.push[index].gdHomeClub ?? ''),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image.network(
                                  laligaProvider.push[index].imgEnemyClub ?? '',
                                  fit: BoxFit.contain,
                                  width: 25,
                                  height: 25,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(laligaProvider.push[index].nameEnemyClub ??
                                    ''),
                              ],
                            ),
                            Text(laligaProvider.push[index].gdEnemyClub ?? ''),
                          ],
                        ),
                      ],
                    ),
                  );
                },
              ),
            ],
          )
      );
    },
    );
  }
}
