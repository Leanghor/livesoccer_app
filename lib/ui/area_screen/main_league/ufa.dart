import 'package:flutter/material.dart';
import 'package:flutter_demo3/provider/ufa_provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class UFA extends StatefulWidget {
  const UFA({super.key});

  @override
  State<UFA> createState() => _UFAState();
}

class _UFAState extends State<UFA> {
  @override
  void initState() {
    Provider.of<EuFaProvider>(context, listen: false).getPostData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<EuFaProvider>(
      builder: (context, provider, child) {
        return Container(
          height: 150,
          margin: const EdgeInsets.only(left: 8, top: 8),
          child: provider.isLoading
              ? const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Center(
                      child: SpinKitFadingCircle(
                        color: Colors.black,
                        size: 35.0,
                      )
                    ),
                    Text(
                      'Loading...',
                      style: TextStyle(color: Colors.red),
                    )
                  ],
                )
              : ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: provider.data.length,
                  itemBuilder: (context, index) {
                    return Container(
                      width: 300,
                      height: 150,
                      margin: const EdgeInsets.only(right: 10),
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        border: Border.all(width: 2, color: Colors.green),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        children: [
                          const Row(
                            children: [
                              CircleAvatar(
                                radius: 20,
                                backgroundImage:
                                    AssetImage('assets/images/R (1).jpeg'),
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Credit: UEFA & EURO',
                                      style: TextStyle(color: Colors.blue)),
                                  Text('UEFA Champion League 2023 '),
                                ],
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Column(
                                children: [
                                  CircleAvatar(
                                    radius: 20,
                                    backgroundColor: Colors.white,
                                    child: Image.network(
                                      provider.data[index].imageHomeClub ?? '',
                                      fit: BoxFit.contain,
                                      height: 25,
                                      width: 25,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(provider.data[index].homeNameClub
                                      .toString()),
                                ],
                              ),
                              Column(
                                children: [
                                  Text(
                                      "${provider.data[index].gdHomeClub.toString()}"
                                      r" Vs "
                                      "${provider.data[index].gdEnemyClub.toString()}"),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  const Text(
                                    'Pending...',
                                    style: TextStyle(color: Colors.red),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  CircleAvatar(
                                    radius: 20,
                                    backgroundColor: Colors.white,
                                    child: Image.network(
                                      provider.data[index].imageEnemyClub
                                          .toString(),
                                      fit: BoxFit.cover,
                                      height: 25,
                                      width: 25,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(provider.data[index].enemyNameClub
                                      .toString()),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                ),
        );
      },
    );
  }
}
