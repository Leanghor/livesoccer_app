import 'package:flutter/material.dart';

class SoccerScreen extends StatefulWidget {
  const SoccerScreen({super.key});

  @override
  State<SoccerScreen> createState() => _SoccerScreenState();
}

class _SoccerScreenState extends State<SoccerScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: scaffoldKey,
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: const EdgeInsets.only(top: 5),
        child: Column(
          children: [
            const Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Text('Live Match', style: TextStyle(
                        decoration: TextDecoration.underline
                    ),),
                    Text('*', style: TextStyle(
                        color: Colors.red
                    ),),
                  ],
                ),
                Row(
                  children: [
                    Text('See All'),
                    SizedBox(width: 5,),
                    Icon(Icons.arrow_forward_ios, size: 13,)
                  ],
                )
              ],
            ),
            const SizedBox(height: 10,),
            ListView.builder(
              padding: EdgeInsets.zero,
              itemCount: 4,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return Container(
                  margin: const EdgeInsets.only(top: 10),
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Image.network('https://cpl.sgp1.cdn.digitaloceanspaces.com/logo/club/small/1691333334.png', fit: BoxFit.contain, width: 30, height: 30,),
                          const Text('Angkor Tiger FC', style: TextStyle(
                              fontSize: 10
                          ))
                        ],
                      ),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("0 Vs 0")
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Image.network('https://cpl.sgp1.cdn.digitaloceanspaces.com/logo/club/small/1691333371.png', fit: BoxFit.contain, width: 30, height: 30,),
                          const Text('Boeung Ket FC', style: TextStyle(
                              fontSize: 10
                          ))
                        ],
                      )
                    ],
                  ),
                );
              },
            ),

            const Divider(height: 10, color: Colors.black,),
            const SizedBox(height: 10,),
            const Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Text('Upcoming Match', style: TextStyle(
                        decoration: TextDecoration.underline
                    ),),
                    Text('*', style: TextStyle(
                        color: Colors.red
                    ),),
                  ],
                ),
                Row(
                  children: [
                    Text('See All'),
                    SizedBox(width: 5,),
                    Icon(Icons.arrow_forward_ios, size: 13,)
                  ],
                )
              ],
            ),

            const SizedBox(height: 10,),
            ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: 4,
              padding: EdgeInsets.zero,
              itemBuilder: (context, index) {
                return Container(
                  margin: const EdgeInsets.only(top: 5),
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Image.network('https://cpl.sgp1.cdn.digitaloceanspaces.com/logo/club/small/1691333334.png', fit: BoxFit.contain, width: 30, height: 30,),
                          const Text('Angkor Tiger FC', style: TextStyle(
                              fontSize: 10
                          ))
                        ],
                      ),
                      const Column(
                        children: [
                          Text("Mon, 30 Oct 2023", style: TextStyle(
                            fontSize: 10
                          )),
                          Text("Soon", style: TextStyle(
                              color: Colors.green,
                              fontSize: 10
                          ),)
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Image.network('https://cpl.sgp1.cdn.digitaloceanspaces.com/logo/club/small/1691333371.png', fit: BoxFit.contain, width: 30, height: 30,),
                          const Text('Boeung Ket FC' , style: TextStyle(
                              fontSize: 10
                          ))
                        ],
                      )
                    ],
                  ),
                );
              },
            ),
            const SizedBox(height: 5,)
          ],
        ),
      ),
    );
  }
}
