import 'package:flutter/material.dart';
import 'package:flutter_demo3/no_data.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CPLScreen extends StatelessWidget {
  CPLScreen({super.key});

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: scaffoldKey,
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 10.0),
                  child: Text('Cambodia Premier League 2023', style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),),
                ),
                SvgPicture.asset('assets/icons/trophy.svg', width: 30, height: 30, fit: BoxFit.cover,)
              ],
            ),
            // ListView.builder(
            //   shrinkWrap: true,
            //   physics: const NeverScrollableScrollPhysics(),
            //   padding: EdgeInsets.zero,
            //   itemCount: 8,
            //   itemBuilder: (context, index) {
            //     return Stack(
            //       children: [
            //         Container(
            //           width: MediaQuery.of(context).size.width,
            //           height: 300,
            //           padding: const EdgeInsets.all(10),
            //           decoration: const BoxDecoration(
            //               image: DecorationImage(
            //                   image: NetworkImage('https://th.bing.com/th/id/R.b07fa8d545782d8770b4606a63a4b1f8?rik=M5ni4ljQCmgqEw&riu=http%3a%2f%2fwww.boeungketfc.com%2fwp-content%2fuploads%2f2020%2f07%2f115844448_3429914363720300_3378954982495608797_o.jpg&ehk=XLGa6Kyiy%2bQNY3GfeEwhNJRy9fd8ET4Zi4NEbtwBjWE%3d&risl=&pid=ImgRaw&r=0')
            //               )
            //           ),
            //         ),
            //         Positioned(
            //           child: Container(
            //             margin: const EdgeInsets.only(top: 205),
            //             padding: const EdgeInsets.all(10),
            //             width: MediaQuery.of(context).size.width,
            //             decoration: BoxDecoration(
            //                 color: Colors.black.withOpacity(0.4)
            //             ),
            //             child: Row(
            //               children: [
            //                 Image.network('https://cpl.sgp1.cdn.digitaloceanspaces.com/logo/club/small/1691333371.png', width: 50, height: 50,),
            //                 const Column(
            //                   crossAxisAlignment: CrossAxisAlignment.start,
            //                   children: [
            //                     Text('Boeung Ket FC', style: TextStyle(
            //                        color: Colors.white,
            //                       fontWeight: FontWeight.bold
            //                     )),
            //                     Text('Staduim: NATIONAL OLYMPIC STADIUM', style: TextStyle(
            //                         color: Colors.white,
            //                       fontWeight: FontWeight.bold
            //                     )),
            //                   ],
            //                 )
            //               ],
            //               )
            //         ),)
            //       ],
            //     );
            // },)
            SizedBox(
              child: const InvalidDataScreen(),
            )
          ],
        ),
      ),
    );
  }
}
