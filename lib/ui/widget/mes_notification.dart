import 'package:flutter/material.dart';

class MesNotify extends StatelessWidget {
  const MesNotify({super.key});

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}
ScaffoldFeatureController<SnackBar, SnackBarClosedReason> userLoginFail(BuildContext context){
  return ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      backgroundColor: Colors.transparent,
      elevation: 0, // Remove shadow
      duration: const Duration(seconds: 4), // Adjust duration as needed
      content: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
        decoration: BoxDecoration(
          color: Colors.redAccent,
          borderRadius: BorderRadius.circular(8),
          boxShadow: const [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 5.0,
              spreadRadius: 1.0,
              offset: Offset(0.0, 2.0),
            ),
          ],
        ),
        child: const Row(
          children: [
            Icon(
              Icons.error,
              color: Colors.white,
            ),
            SizedBox(width: 8),
            Expanded(
              child: Text(
                'Login failed. Please try again.',
                style: TextStyle(color: Colors.white),
              ),
            ),
            Icon(
              Icons.close,
              color: Colors.white,
            ),
          ],
        ),
      ),
    ),
  );
}

