import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo3/provider/login_provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class PreviewProfilePicture extends StatefulWidget {
  const PreviewProfilePicture({Key? key, required this.imagePreview}) : super(key: key);

  final String? imagePreview;

  @override
  State<PreviewProfilePicture> createState() => _PreviewProfilePictureState();
}

class _PreviewProfilePictureState extends State<PreviewProfilePicture> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<LoginProviderController>(context, listen: false);
    // pro.removeUserImage(pro.userModel!.id.toString(), listenUpdate: true);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Consumer<LoginProviderController>(
        builder: (context, provider, child) {
          return CustomScrollView(
            slivers: [
              SliverPadding(
                padding: const EdgeInsets.only(right: 10),
                sliver: SliverAppBar(
                  backgroundColor: Colors.black,
                  leading: InkWell(
                    borderRadius: BorderRadius.circular(50),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    child: const Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: 16,
                    ),
                  ),
                  centerTitle: true,
                  title: const Text("1 of 1",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.bold)),
                  actions: [
                    SvgPicture.asset(
                      "assets/icons/link-outline.svg",
                      color: Colors.white,
                      width: 25,
                      height: 25,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    GestureDetector(
                      onTap: () {
                        showCupertinoModalPopup(
                          context: context,
                          builder: (BuildContext context) =>
                            CupertinoActionSheet(
                              cancelButton: CupertinoActionSheetAction(
                                isDefaultAction: true,
                                onPressed: () {
                                  Navigator.pop(context, 'Cancel');
                                },
                                child: const Text(
                                  'Cancel',
                                  style: TextStyle(color: Colors.red),
                                ),
                              ),
                              actions: <Widget>[
                                CupertinoActionSheetAction(
                                  child: const Text(
                                    'Delete',
                                    style: TextStyle(color: Colors.red),
                                  ),
                                  onPressed: () async {
                                    try{
                                      provider.removeUserImage(provider.userModel!.id.toString(), listenUpdate: true);
                                      setState(() {});
                                    }catch(_){}finally{
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                    }
                                  }
                                ),
                                CupertinoActionSheetAction(
                                  child: const Text('Share Photo',
                                      style: TextStyle(
                                          color: Colors.blue)),
                                  onPressed: () async {},
                                ),
                                CupertinoActionSheetAction(
                                  child: const Text('Save Photo',
                                    style: TextStyle(
                                      color: Colors.blue
                                    )
                                  ),
                                  onPressed: () async {},
                                ),
                              ],
                            )
                        );
                      },
                      child: SvgPicture.asset(
                        "assets/icons/ellipsis-horizontal-outline.svg",
                        color: Colors.white,
                        width: 25,
                        height: 25,
                      )
                    )
                  ],
                ),
              ),
              SliverPadding(
                padding: const EdgeInsets.only(top: 70),
                sliver: SliverToBoxAdapter(
                  child: SizedBox(
                    height: 600,
                    child: CachedNetworkImage(
                      imageUrl: widget.imagePreview != null
                        && widget.imagePreview!.isNotEmpty
                        ? "${provider.url}${widget.imagePreview.toString()}"
                        : 'https://i.pinimg.com/736x/a8/57/00/a85700f3c614f6313750b9d8196c08f5.jpg',
                      placeholder: (context, url) =>
                      const SizedBox(
                        width: 30,
                        height: 30,
                        child: Center(
                          child: SpinKitCircle(
                            color: Colors.white,
                            size: 40.0,
                          )
                        )
                      ),
                      errorWidget: (context, url, error) =>
                      const Icon(Icons.error),
                    ),
                  ),
                ),
              ),
              SliverPadding(
                padding: const EdgeInsets.only(right: 10, top: 10),
                sliver: SliverToBoxAdapter(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(provider.profileImage != null ? "Image: 1" : "Image: 0",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.white)
                      ),
                    ],
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
