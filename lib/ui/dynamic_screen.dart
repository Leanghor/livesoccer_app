import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo3/no_data.dart';
import 'package:flutter_demo3/provider/master_data/master_data_provider.dart';
import 'package:flutter_demo3/ui/club/preview_club_screen.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class DynamicLeagueScreen extends StatefulWidget {
  const DynamicLeagueScreen(
      {super.key,
      required this.leagueCountry,
      required this.title,
      required this.imageLogo});

  final String? leagueCountry;
  final String? title;
  final String? imageLogo;

  @override
  State<DynamicLeagueScreen> createState() => _TicketAppState();
}

class _TicketAppState extends State<DynamicLeagueScreen> {
  bool _isInitialized = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (!_isInitialized) {
        Provider.of<MasterDataProvider>(context, listen: false)
            .fetchMLD(nameClub: widget.leagueCountry.toString());
        _isInitialized = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    MasterDataProvider mailData = Provider.of<MasterDataProvider>(context);
    var f = CustomScrollView(
      slivers: [
        SliverPadding(
          padding: const EdgeInsets.all(10),
          sliver: SliverGrid(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 150.0,
              mainAxisSpacing: 15.0,
              crossAxisSpacing: 15.0,
              childAspectRatio: 1.0,
            ),
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
              return InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PreviewClubScreen(
                            detailInformation: mailData.mld[index]),
                      ));
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset:
                              const Offset(0, 3), // changes position of shadow
                        ),
                      ],
                      color: Colors.grey.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(15)),
                  child: mailData.mld.isNotEmpty
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CachedNetworkImage(
                              imageUrl: mailData
                                      .mld[index].clubInformation?[0].logo
                                      .toString() ??
                                  "",
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                margin: const EdgeInsets.all(10),
                                height: 50,
                                width: 50,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  image: DecorationImage(
                                    image: imageProvider,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              placeholder: (context, url) => const SizedBox(
                                width: 30,
                                height: 30,
                                child: Center(
                                  child: CircularProgressIndicator(
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              errorWidget: (context, url, error) =>
                                  const Icon(Icons.error),
                            ),
                            const Text("CLUB",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline)),
                            Text(mailData.mld[index].nameClub ?? "",
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 8)),
                            Text(
                                "Session: ${mailData.mld[index].session ?? ""}",
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 10)),
                          ],
                        )
                      : const SizedBox(),
                ),
              );
            }, childCount: mailData.mld.length),
          ),
        )
      ],
    );
    return RefreshIndicator(
      onRefresh: () {
        return Future.delayed(const Duration(seconds: 1));
      },
      child: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            leading: InkWell(
                borderRadius: BorderRadius.circular(50),
                onTap: () {
                  Navigator.pop(context);
                },
                child: const Icon(
                  Icons.arrow_back_ios,
                  size: 16,
                  color: Colors.white,
                )),
            // expandedHeight: 200.0,
            flexibleSpace: FlexibleSpaceBar(
              background: Container(
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Colors.blue, Colors.green],
                    // Set your gradient colors
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
                ),
              ),
            ),
            title: Text(widget.title.toString(),
                style: GoogleFonts.actor(
                    textStyle: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                        color: Colors.white))),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: CachedNetworkImage(
                  imageUrl: widget.imageLogo.toString(),
                  imageBuilder: (context, imageProvider) => Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  placeholder: (context, url) => const SizedBox(
                      width: 30,
                      height: 30,
                      child: Center(
                        child: CircularProgressIndicator(
                          color: Colors.black,
                        ),
                      )),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
              )
            ],
          ),
          body: mailData.mld.isNotEmpty ? f : const InvalidDataScreen()),
    );
  }
}
