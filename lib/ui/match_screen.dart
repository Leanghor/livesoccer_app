import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo3/no_data.dart';

// ignore: must_be_immutable
class MatchApp extends StatefulWidget {
  MatchApp({super.key});

  @override
  State<MatchApp> createState() => _MatchAppState();

  var nameLeague = [
    {'id': '1', 'color': 'blue', 'name': 'English Premier League 2022/2023'},
    {'id': '2', 'color': 'green', 'name': 'League1 2022/2023'},
    {'id': '3', 'color': 'cyan', 'name': 'Seri A 2022/2023'},
    {'id': '4', 'color': 'red', 'name': 'Bundesliga 2022/2023'},
    {'id': '5', 'color': 'orange', 'name': 'Laliga Santander 2022/2023'}
  ];
}

class _MatchAppState extends State<MatchApp> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  String? _activeId;
  @override
  void initState() {
    super.initState();
    _activeId = widget.nameLeague[0]['id'];
  }

  void _onTap(index) {
    setState(() {
      _activeId = widget.nameLeague[index]['id'];
    });
  }

  final sliderImg = [
    'assets/slide/360_F_247829663_u8MnsUWNoXt9HMZoqlYUXiBOpbh2ILt2.jpg',
    'assets/slide/360_F_295214797_egk0BKwE9YSJc54yIVvHLB6ct2VYWrp1.jpg',
    'assets/slide/depositphotos_164375318-stock-photo-soccer-best-moments-mixed-media.jpg',
    'assets/slide/depositphotos_174558338-stock-photo-goalkeeper-kicks-the-ball-in.jpg',
    'assets/slide/depositphotos_381545532-stock-photo-soccer-players-action-professional-stadium.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: () {
          return Future<void>.delayed(const Duration(seconds: 2));
        },
        child: Scaffold(
            body: CustomScrollView(
          slivers: [
            SliverAppBar(
              automaticallyImplyLeading: false,
              flexibleSpace: FlexibleSpaceBar(
                background: Container(
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.blue,
                        Colors.green
                      ], // Set your gradient colors
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                  ),
                ),
                title: const Text("Information Match"),
                centerTitle: true,
              ),
              actions: [
                InkWell(
                  borderRadius: BorderRadius.circular(50),
                  onTap: () {
                    _refreshIndicatorKey.currentState?.show();
                  },
                  child: Container(
                    margin: const EdgeInsets.all(7),
                    padding: const EdgeInsets.all(8),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(50)),
                    child: const Icon(
                      Icons.refresh_sharp,
                      size: 28,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
            SliverToBoxAdapter(
              child: Column(
                children: [
                  CarouselSlider(
                      options:
                          CarouselOptions(autoPlay: true, viewportFraction: 1),
                      items: sliderImg
                          .map(
                            (i) => Container(
                              margin: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20)),
                              width: MediaQuery.of(context).size.width,
                              height: 200,
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(15),
                                  child: Image.asset(
                                    i,
                                    fit: BoxFit.cover,
                                  )),
                            ),
                          )
                          .toList()),
                  Container(
                    margin: const EdgeInsets.all(10),
                    height: 40,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                      itemCount: widget.nameLeague.length,
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            _onTap(index);
                          },
                          child: Container(
                            padding: const EdgeInsets.only(
                                top: 10, right: 15, bottom: 10, left: 15),
                            margin: const EdgeInsets.only(right: 10),
                            decoration: BoxDecoration(
                                color:
                                    _activeId == widget.nameLeague[index]['id']
                                        ? Colors.lightGreenAccent
                                        : Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(20)),
                            child: Text(
                                widget.nameLeague[index]['name'].toString()),
                          ),
                        );
                      },
                    ),
                  )
                ],
              ),
            ),
            const SliverToBoxAdapter(
              child: InvalidDataScreen(),
            )
          ],
        )));
  }
}
