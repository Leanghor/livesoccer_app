import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo3/globle/globle_key.dart';
import 'package:flutter_demo3/no_data.dart';
import 'package:flutter_demo3/provider/login_provider.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:material_dialogs/widgets/buttons/icon_button.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({super.key});

  @override
  State<NotificationScreen> createState() => _NotificationState();
}

class _NotificationState extends State<NotificationScreen> {
  bool isDeleting = false;
  String? token = "";
  String createdAtStr = "";
  String? formattedDateTime;

  @override
  void initState() {
    super.initState();
    setState(() {
      token = storeToken.$;
    });
    Future.delayed(const Duration(seconds: 2), () {});
  }

  @override
  Widget build(BuildContext context) {
    LoginProviderController value =
        Provider.of<LoginProviderController>(context);
    var f = CustomScrollView(
      slivers: [
        const SliverToBoxAdapter(
          child: SizedBox(height: 10,),
        ),
        SliverPadding(
          padding: const EdgeInsets.only(left: 5, right: 5),
          sliver: SliverGrid(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 500.0,
              mainAxisSpacing: 10.0,
              crossAxisSpacing: 10.0,
              childAspectRatio: 4.0,
            ),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                if (value.userModel!.messages != null && value.userModel!.messages![index].createdAt!.isNotEmpty) {
                  createdAtStr = value.userModel!.messages![index].createdAt.toString();
                  DateTime createdAt = DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'")
                      .parse(createdAtStr, true);
                  formattedDateTime = DateFormat('yyyy-MM-dd').format(createdAt);
                } else {
                  formattedDateTime = "null";
                }
              return Container(
                padding: const EdgeInsets.only(left: 10, right: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.grey.withOpacity(0.2),
                  border: const Border(
                      bottom: BorderSide(color: Colors.teal)
                  )
                ),
                child: Row(
                children: [
                  Stack(
                    children: [
                      Container(
                        width: 75,
                        height: 75,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                          border: Border.all(
                            width: 2,
                            color: Colors.blueAccent
                          )
                        ),
                        child: ClipOval(
                          child: CachedNetworkImage(
                            imageUrl: value.userModel != null && value.userModel!.profileImage!.isNotEmpty && value.userModel.toString() != "" ? value.userModel!.profileImage.toString() :
                            'https://i.pinimg.com/736x/a8/57/00/a85700f3c614f6313750b9d8196c08f5.jpg',
                            placeholder: (context, url) => const SizedBox(
                                width: 30,
                                height: 30,
                                child: Center(
                                  child: CircularProgressIndicator(
                                    color: Colors.blue,
                                  ),
                                )
                            ),
                            errorWidget: (context, url, error) =>
                            const Icon(Icons.error),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        right: 0,
                        child: Container(
                          padding: const EdgeInsets.all(3),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.grey.withOpacity(0.2),
                              width: 2
                            ),
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: const Icon(Icons.message_outlined, color: Colors.blue, size: 15,)
                        )
                      )
                    ],
                  ),
                  const SizedBox(width: 10,),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(userName.$, style: const TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.underline,
                            decorationColor: Colors.black
                          ),),
                          SizedBox(
                            width: 180,
                            child: Text(r" have an message: " "${value.userModel!.messages![index].notification}",
                              style: const TextStyle(
                                color: Colors.black,
                                decorationColor: Colors.black
                            ), overflow: TextOverflow.ellipsis, ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          const Text("Date: ", style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.underline,
                            decorationColor: Colors.black
                          ),),
                          SizedBox(
                            width: 180,
                            child: Text(value.userModel!.messages![index].createdAt.toString(), style: const TextStyle(
                              color: Colors.black,
                              decorationColor: Colors.black
                            ), overflow: TextOverflow.ellipsis,),
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(width: 20,),
                  InkWell(
                    borderRadius: BorderRadius.circular(50),
                    onTap: (){
                      showMaterialModalBottomSheet(
                        context: context,
                        builder: (context) => SingleChildScrollView(
                          controller: ModalScrollController.of(context),
                          child: Container(
                            margin: const EdgeInsets.all(10),
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20)
                              )
                            ),
                            child: Column(
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(bottom: 10),
                                  width: 50,
                                  height: 5,
                                  decoration: BoxDecoration(
                                    color: Colors.grey.withOpacity(0.5),
                                    borderRadius: BorderRadius.circular(20)
                                  ),
                                ),
                                Stack(
                                  children: [
                                    Container(
                                      width: 90,
                                      height: 90,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(50),
                                          border: Border.all(
                                              width: 2,
                                              color: Colors.blueAccent
                                          )
                                      ),
                                      child: ClipOval(
                                        child: CachedNetworkImage(
                                          imageUrl: value.userModel != null && value.userModel!.profileImage!.isNotEmpty && value.userModel.toString() != "" ? value.userModel!.profileImage.toString() :
                                          'https://i.pinimg.com/736x/a8/57/00/a85700f3c614f6313750b9d8196c08f5.jpg',
                                          placeholder: (context, url) => const SizedBox(
                                              width: 30,
                                              height: 30,
                                              child: Center(
                                                child: CircularProgressIndicator(
                                                  color: Colors.blue,
                                                ),
                                              )
                                          ),
                                          errorWidget: (context, url, error) =>
                                          const Icon(Icons.error),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 0,
                                      right: 0,
                                      child: Container(
                                        padding: const EdgeInsets.all(3),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.grey.withOpacity(0.2),
                                            width: 2
                                          ),
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(50),
                                        ),
                                        child: const Icon(Icons.message_outlined, color: Colors.blue, size: 15,)
                                      )
                                    )
                                  ],
                                ),
                                const SizedBox(height: 10),
                                const Divider(color: Colors.grey, height: 3),
                                const SizedBox(height: 20),
                                const Text("Confirm!", style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontSize: 18
                                ),),
                                Text(r"Remove message: " " ''${value.userModel!.messages![index].notification.toString()}'' "),

                                const SizedBox(height: 10),
                                IconsButton(
                                  color: Colors.red,
                                  iconData: Icons.remove_circle,
                                  onPressed: () async {
                                    _showLoadingDialog(context);
                                    await Future.delayed(const Duration(seconds: 1));
                                    setState(() {
                                      value.deleteMessageData(value.userModel!.messages!.first.id.toString());
                                    });
                                    if(!context.mounted) return;
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Provider.of<LoginProviderController>(context, listen: false).getUserInformation();
                                  },
                                  text: "Remove",
                                  textStyle: const TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      decorationColor: Colors.white,
                                      decoration: TextDecoration.underline
                                  ),
                                ),
                                const SizedBox(height: 20),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                    child: Container(
                      alignment: Alignment.center,
                      width: 50,
                      height: 50,
                      child: SvgPicture.asset(
                        "assets/icons/ellipsis-horizontal-outline.svg",
                        width: 30,
                        height: 30,
                      ),
                    ),
                  )
                ],
              ),
            );
            },
            childCount: value.userModel != null ? value.userModel!.messages!.length : 0
          ),
        )
        )],
    );
    return RefreshIndicator(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: FlexibleSpaceBar(
            background: Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Colors.blue,
                    Colors.green
                  ], // Set your gradient colors
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
            ),
          ),
          centerTitle: true,
          title: const Text("Notification",
            style: TextStyle(
              fontSize: 18,
              decorationColor: Colors.white,
              color: Colors.white,
              fontWeight: FontWeight.bold)),
          leading: InkWell(
            borderRadius: BorderRadius.circular(50),
            onTap: () {
              Navigator.pop(context);
            },
            child: const Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.white,
            )),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Stack(
                children: [
                  Container(
                    alignment: Alignment.center,
                    width: 30,
                    height: 30,
                    decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(50)),
                    child: const Icon(Icons.notifications_active_sharp, color: Colors.black),
                  ),
                  if(value.userModel != null && value.userModel!.messages!.isNotEmpty
                      && value.userModel!.messages!.toString() != "")...[
                    Positioned(
                      right: 3,
                      top: 0,
                      child: Container(
                        width: 13,
                        height: 13,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius:
                          BorderRadius.circular(50),
                          color: Colors.red,
                        ),
                        child: Text(
                          value.userModel!.messages!.length.toString(),
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 7,
                          ),
                        )
                      )
                    )
                  ],
                ],
              ),
            )
          ],
        ),
        body: value.userModel != null && value.userModel!.messages!.isNotEmpty ? f : const InvalidDataScreen()
      ),
      onRefresh: () {
        return Future<void>.delayed(const Duration(seconds: 1));
      }
    );
  }
}

Future<void> _showLoadingDialog(BuildContext context) async {
  return showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return Container(
        alignment: Alignment.center,
        width: 100,
        height: 100,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15)),
        child: Container(
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: const CircularProgressIndicator(
            color: Colors.blueAccent,
          ),
        )
      );
    },
  );
}
