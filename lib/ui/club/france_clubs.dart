import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FranceClubsScreen extends StatelessWidget {
  const FranceClubsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverPadding(
          padding: const EdgeInsets.all(10),
          sliver: SliverGrid(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 150.0,
              mainAxisSpacing: 10.0,
              crossAxisSpacing: 10.0,
              childAspectRatio: 1.0,
            ),
            delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                return Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.blue.withOpacity(0.1),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/images/13.png",
                        fit: BoxFit.cover,
                        width: 50,
                        height: 50,
                      ),
                      const SizedBox(height: 10,),
                      Center(
                        child: Text(
                          'Paris \n Saint-Germain',
                          style: GoogleFonts.actor(
                              textStyle: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              )),
                        ),
                      )
                    ],
                  ),
                );
              },
              childCount: 20,
            ),
          ),
        ),
      ],
    );
  }
}
