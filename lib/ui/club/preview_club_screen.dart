import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_demo3/models/master_premier_model.dart';

class PreviewClubScreen extends StatefulWidget {
  const PreviewClubScreen({super.key, required this.detailInformation});

  final MasterModel? detailInformation;

  @override
  State<PreviewClubScreen> createState() => _PreviewClubScreenState();
}

class _PreviewClubScreenState extends State<PreviewClubScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 50,
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Colors.blue,
                            Colors.green
                          ], // Set your gradient colors
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                      ),
                    ),
                    for (var c
                        in widget.detailInformation!.clubInformation!) ...[
                      ClipPath(
                        clipper: OvalBottomBorderClipper(),
                        child: Stack(
                          children: [
                            Container(
                              alignment: Alignment.center,
                              height: 300,
                              padding:
                                  const EdgeInsets.only(left: 20, right: 20),
                              width: double.maxFinite,
                              decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                  image: DecorationImage(
                                      fit: BoxFit.contain,
                                      image: NetworkImage(
                                          "https://talksport.com/wp-content/uploads/sites/5/2020/07/NINTCHDBPICT000575315030.jpg?strip=all&quality=100&w=1200&h=800&crop=1"))),
                            ),
                            BackdropFilter(
                              filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                              // Adjust the sigma values for the desired blur intensity
                              child: Container(
                                color: Colors.transparent,
                              ),
                            ),
                            Positioned(
                                left: 110,
                                child: CachedNetworkImage(
                                  imageUrl: c.cover.toString(),
                                  imageBuilder: (context, imageProvider) =>
                                      Container(
                                    height: 300,
                                    width: 200,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: imageProvider,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  placeholder: (context, url) => const SizedBox(
                                    width: 30,
                                    height: 30,
                                    child: Center(
                                      child: CircularProgressIndicator(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  errorWidget: (context, url, error) =>
                                      const Icon(Icons.error),
                                )),
                            Positioned(
                                left: 10,
                                child: InkWell(
                                    borderRadius: BorderRadius.circular(50),
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: const SizedBox(
                                        width: 40,
                                        height: 50,
                                        child: Icon(
                                          Icons.arrow_back_ios,
                                          color: Colors.white,
                                          size: 16,
                                        ))))
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(
                            left: 20.0, top: 5, bottom: 20),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                    widget.detailInformation!.nameClub
                                        .toString(),
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                            const Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text("Current information.",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    )),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                    Container(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Column(
                        children: [
                          const Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Last 5 matches.",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                      fontSize: 18)),
                            ],
                          ),
                          Visibility(
                            visible: widget.detailInformation!.won!.length <= 1
                                ? false
                                : true,
                            child: SizedBox(
                              height: 40,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount:
                                    widget.detailInformation!.won!.length - 1,
                                itemBuilder: (context, index) {
                                  return Row(
                                    children: [
                                      Container(
                                        width: 40,
                                        height: 40,
                                        alignment: Alignment.center,
                                        padding: const EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                          color: Colors.lightGreen,
                                          borderRadius:
                                              BorderRadius.circular(50),
                                        ),
                                        child: const Text("W"),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      )
                                    ],
                                  );
                                },
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Visibility(
                            visible: widget.detailInformation!.lost!.length <= 1
                                ? false
                                : true,
                            child: SizedBox(
                              height: 40,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount:
                                    widget.detailInformation!.lost!.length - 1,
                                itemBuilder: (context, index) {
                                  return Row(
                                    children: [
                                      Container(
                                        width: 40,
                                        height: 40,
                                        alignment: Alignment.center,
                                        padding: const EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                          color: Colors.red,
                                          borderRadius:
                                              BorderRadius.circular(50),
                                        ),
                                        child: const Text("L"),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      )
                                    ],
                                  );
                                },
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Visibility(
                            visible:
                                widget.detailInformation!.drawn!.length <= 1
                                    ? false
                                    : true,
                            child: SizedBox(
                              height: 40,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount:
                                    widget.detailInformation!.drawn!.length - 1,
                                itemBuilder: (context, index) {
                                  return Row(
                                    children: [
                                      Container(
                                        width: 40,
                                        height: 40,
                                        alignment: Alignment.center,
                                        padding: const EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                          color: Colors.grey,
                                          borderRadius:
                                              BorderRadius.circular(50),
                                        ),
                                        child: const Text("D"),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      )
                                    ],
                                  );
                                },
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                decoration: const BoxDecoration(
                                    color: Colors.lightGreen,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                                margin: const EdgeInsets.only(top: 10),
                                padding:
                                    const EdgeInsets.only(left: 10, right: 10),
                                child: const Text("W = Win",
                                    style: TextStyle(color: Colors.white)),
                              ),
                              Container(
                                decoration: const BoxDecoration(
                                    color: Colors.red,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                                margin: const EdgeInsets.only(top: 10, left: 5),
                                padding:
                                    const EdgeInsets.only(left: 10, right: 10),
                                child: const Text("L = Lost",
                                    style: TextStyle(color: Colors.white)),
                              ),
                              Container(
                                decoration: const BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                                margin: const EdgeInsets.only(top: 10, left: 5),
                                padding:
                                    const EdgeInsets.only(left: 10, right: 10),
                                child: const Text("D = Drawn",
                                    style: TextStyle(color: Colors.white)),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          for (var i in widget
                              .detailInformation!.clubInformation!) ...[
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(r"Table rank: "
                                    "${widget.detailInformation!.id}"),
                              ],
                            ),
                            Table(
                                border: TableBorder.all(),
                                // Allows to add a border decoration around your table
                                children: [
                                  TableRow(children: [
                                    const Center(
                                        child: Text(
                                      'Club',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )),
                                    Center(
                                        child: Text(
                                            widget.detailInformation!.nameClub
                                                .toString(),
                                            style: const TextStyle(
                                                fontWeight: FontWeight.bold))),
                                  ]),
                                  TableRow(children: [
                                    const Center(child: Text('Played')),
                                    Center(child: Text(i.mp.toString())),
                                  ]),
                                  TableRow(children: [
                                    const Center(child: Text('Won')),
                                    Center(child: Text(i.won.toString())),
                                  ]),
                                  TableRow(children: [
                                    const Center(child: Text('Drawn')),
                                    Center(child: Text(i.drawn.toString())),
                                  ]),
                                  TableRow(children: [
                                    const Center(child: Text('Lost')),
                                    Center(child: Text(i.lost.toString())),
                                  ]),
                                  TableRow(children: [
                                    const Center(child: Text('GF')),
                                    Center(child: Text(i.gf.toString())),
                                  ]),
                                  TableRow(children: [
                                    const Center(child: Text('GA')),
                                    Center(child: Text(i.ga.toString())),
                                  ]),
                                  TableRow(children: [
                                    const Center(child: Text('GD')),
                                    Center(child: Text(i.gd.toString())),
                                  ]),
                                  TableRow(children: [
                                    const Center(child: Text('Point')),
                                    Center(child: Text(i.pt.toString())),
                                  ]),
                                ]),
                          ]
                        ],
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
