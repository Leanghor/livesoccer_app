import 'package:flutter/material.dart';
import 'package:flutter_demo3/no_data.dart';

class MessageScreen extends StatefulWidget {
  const MessageScreen({super.key});

  @override
  State<MessageScreen> createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(child: Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            automaticallyImplyLeading: false,
            flexibleSpace: FlexibleSpaceBar(
              background: Container(
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.blue,
                      Colors.green
                    ], // Set your gradient colors
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
                ),
              ),
            ),
            centerTitle: true,
            title: const Text("Your Feed Back",
              style: TextStyle(
                fontSize: 16,
                decoration: TextDecoration.underline,
                decorationColor: Colors.white,
                color: Colors.white,
                fontWeight: FontWeight.bold
              )),
            leading: InkWell(
              borderRadius: BorderRadius.circular(50),
                onTap: () {
                  Navigator.pop(context);
                },
                child: const Icon(
                  Icons.arrow_back_ios,
                  size: 16,
                  color: Colors.white,
                )),
          ),
          const SliverToBoxAdapter(
            child: InvalidDataScreen()
            ),
        ],
      ),
    ), onRefresh: (){
      return Future<void>.delayed(const Duration(seconds: 1));
    });
  }
}
