import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo3/provider/login_provider.dart';
import 'package:flutter_demo3/security_screen/register_app.dart';
import 'package:flutter_demo3/ui/edit_profile.dart';
import 'package:flutter_demo3/ui/policy_screen.dart';
import 'package:flutter_demo3/ui/preview_profile.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';
import 'package:material_dialogs/material_dialogs.dart';
import 'package:material_dialogs/widgets/buttons/icon_button.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  File? imagePro;
  late File? imageFile;
  String myString = "";
  var d = "";

  @override
  void initState() {
    super.initState();
    Provider.of<LoginProviderController>(context, listen: false).getUserInformation();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LoginProviderController>(
      builder: (context, values, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: Column(children: [
              Stack(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: 200,
                    child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      width: MediaQuery.of(context).size.width,
                      height: 250,
                      imageUrl: values.userModel != null &&
                              values.userModel!.coverImage != null &&
                              values.userModel!.coverImage.toString() != "" &&
                              values.userModel!.coverImage.toString().isNotEmpty
                          ? values.userModel!.coverImage.toString()
                          : 'https://th.bing.com/th/id/R.c4d4f15de3f17bfd4e77c05f27360ee5?rik=QDbBKPPfiOB8CQ&riu=http%3a%2f%2fwallpapercave.com%2fwp%2fH2kIEbg.jpg&ehk=4LhMBo3gArtaGiJSDu0GaetNHHiHpMWMAkluauPISlk%3d&risl=&pid=ImgRaw&r=0',
                      placeholder: (context, url) => const SizedBox(
                          width: 30,
                          height: 30,
                          child: Center(
                            child: CircularProgressIndicator(
                              color: Colors.blue,
                            ),
                          )),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                    ),
                  ),
                  Positioned(
                      top: 80,
                      left: 150,
                      child: Text('profile'.toUpperCase(),
                          style: GoogleFonts.aclonica(
                            textStyle: const TextStyle(
                              fontSize: 24,
                              color: Colors.green,
                            ),
                          ))),
                  Positioned(
                    top: 50,
                    right: 25,
                    child: Container(
                      padding: const EdgeInsets.only(
                          top: 4, bottom: 4, left: 10, right: 10),
                      decoration: BoxDecoration(
                          color: Colors.black.withOpacity(0.5),
                          borderRadius: BorderRadius.circular(5)),
                      child: Text(values.userModel != null
                            && values.userModel.toString() != ""
                            ? values.userModel!.role.toString()
                            : "No",
                        style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(child: Center(
                    child: Container(
                      alignment: Alignment.center,
                      margin:
                          const EdgeInsets.only(top: 150, left: 20, right: 20),
                      padding: const EdgeInsets.only(
                          top: 5, right: 20, left: 20, bottom: 5),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue.shade900.withOpacity(0.2),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              showCupertinoModalPopup(
                                context: context,
                                builder: (BuildContext context) =>
                                    CupertinoActionSheet(
                                      cancelButton: CupertinoActionSheetAction(
                                        isDefaultAction: true,
                                        onPressed: () {
                                          Navigator.pop(context, 'Cancel');
                                        },
                                        child: const Text(
                                          'Cancel',
                                          style: TextStyle(color: Colors.red),
                                        ),
                                      ),
                                      actions: <Widget>[
                                        Visibility(visible: values.userModel != null
                                            && values.userModel!.profileImage != null
                                          && values.userModel!.profileImage.toString().isNotEmpty
                                          ? true
                                          : false,
                                          child: CupertinoActionSheetAction(
                                            child: Row(
                                              children: [
                                                Container(
                                                  width: 40,
                                                  height: 40,
                                                  padding: const EdgeInsets.all(6),
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(50),
                                                    color: Colors.grey.withOpacity(0.2),
                                                  ),
                                                  child: SvgPicture.asset(
                                                    "assets/icons/person-circle-outline.svg",
                                                    color: Colors.blue,
                                                    width: 25,
                                                    height: 25,
                                                  ),
                                                ),
                                                const SizedBox(width: 10),
                                                const Text('See profile picture',
                                                  style: TextStyle(color: Colors.blue),
                                                ),
                                              ],
                                            ),
                                            onPressed: () async {
                                              Navigator.push(context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                    PreviewProfilePicture(
                                                      imagePreview: values.userModel!.profileImage.toString()
                                                    )
                                                )
                                              );
                                          }
                                        ),
                                      ),
                                      CupertinoActionSheetAction(
                                        child: Row(
                                          children: [
                                            Container(
                                              width: 40,
                                              height: 40,
                                              padding: const EdgeInsets.all(6),
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                                color:
                                                    Colors.grey.withOpacity(0.2),
                                              ),
                                              child: SvgPicture.asset(
                                                "assets/icons/image-outline.svg",
                                                color: Colors.blueAccent,
                                                width: 25,
                                                height: 25,
                                              ),
                                            ),
                                            const SizedBox(
                                              width: 10,
                                            ),
                                            const Text(
                                              'From Gallery',
                                              style: TextStyle(color: Colors.blue),
                                            ),
                                          ],
                                        ),
                                        onPressed: () async {
                                          await values.getImageFromGallery().then((_) => {
                                            setState(() {})
                                          });
                                          if (!context.mounted) return;
                                          Navigator.pop(context);
                                        },
                                      ),
                                      CupertinoActionSheetAction(
                                        child: Row(
                                          children: [
                                            Container(
                                              width: 40,
                                              height: 40,
                                              padding: const EdgeInsets.all(6),
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(50),
                                                color: Colors.grey.withOpacity(0.2),
                                              ),
                                              child: const Icon(
                                                Icons.camera_alt,
                                                color: Colors.red,
                                              ),
                                            ),
                                            const SizedBox(
                                              width: 10,
                                            ),
                                            const Text(
                                              'From Camera',
                                              style: TextStyle(color: Colors.red),
                                            ),
                                          ],
                                        ),
                                        onPressed: () async {
                                          await values.getImageFromCamera().then((_) => {
                                            setState(() {})
                                          });
                                          if (!context.mounted) return;
                                          Navigator.pop(context);
                                        },
                                    ),
                                  ],
                                ),
                              );
                            },
                            child: SizedBox(
                              width: 90, // Define a fixed width
                              height: 90, // Define a fixed height
                              child: Stack(
                                children: [
                                  Container(
                                    width: 90,
                                    height: 90,
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(50)),
                                    child: ClipOval(
                                      child: CachedNetworkImage(
                                        fit: BoxFit.cover,
                                        imageUrl: values.userModel != null &&
                                                values.userModel!.profileImage != null &&
                                                values.userModel.toString().isNotEmpty
                                            ? "${values.url}${values.userModel!.profileImage.toString()}"
                                            : 'https://i.pinimg.com/736x/a8/57/00/a85700f3c614f6313750b9d8196c08f5.jpg',
                                        placeholder: (context, url) =>
                                          const SizedBox(
                                            width: 30,
                                            height: 30,
                                            child: Center(
                                              child: CircularProgressIndicator(
                                                color: Colors.blue,
                                              ),
                                            )
                                          ),
                                        errorWidget: (context, url, error) => const Icon(Icons.error),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    bottom: 0,
                                    right: 0,
                                    child: Container(
                                      width: 30,
                                      height: 30,
                                      decoration: BoxDecoration(
                                        border: Border.all(color: Colors.white),
                                        borderRadius: BorderRadius.circular(50),
                                        color: Colors.black.withOpacity(0.7),
                                      ),
                                      child: const Icon(
                                        Icons.camera_alt,
                                        size: 15,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                  values.userModel != null &&
                                          values.userModel!.name != null
                                      ? values.userModel!.name.toString()
                                      : "Username",
                                  style: const TextStyle(fontSize: 18)),
                              Text(
                                  values.userModel != null &&
                                          values.userModel!.email != null
                                      ? values.userModel!.email.toString()
                                      : "Email",
                                  overflow: TextOverflow.ellipsis),
                              Row(
                                children: [
                                  ElevatedButton(
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              const EditProfileScreen(),
                                        )
                                      );
                                    },
                                    style: ElevatedButton.styleFrom(
                                      foregroundColor: Colors.white,
                                      backgroundColor: Colors.teal,
                                      disabledForegroundColor:
                                          Colors.grey.withOpacity(0.38),
                                      disabledBackgroundColor:
                                          Colors.grey.withOpacity(0.12),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(7),
                                        // Use RoundedRectangleBorder for custom shape
                                        side: const BorderSide(
                                            color: Colors.teal,
                                            width: 2), // Add a border
                                      ),
                                    ),
                                    child: const Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Icon(
                                          Icons.edit,
                                          color: Colors.white,
                                        ),
                                        SizedBox(width: 5),
                                        // Add some spacing between icon and text
                                        Text('Edit Profile'),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  const Text('10+k')
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ))
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(
                  top: 20,
                  left: 20,
                  right: 20,
                ),
                padding: const EdgeInsets.only(
                    top: 10, bottom: 10, left: 10, right: 5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.grey.withOpacity(0.1)),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(Icons.history),
                        SizedBox(
                          width: 20,
                        ),
                        Text('History',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                      ],
                    ),
                    Row(
                      children: [
                        Icon(Icons.arrow_forward_ios),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
                padding: const EdgeInsets.only(
                    top: 10, bottom: 10, left: 10, right: 5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.grey.withOpacity(0.1)),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(Icons.apps_outage),
                        SizedBox(
                          width: 20,
                        ),
                        Text('More App',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                      ],
                    ),
                    Row(
                      children: [
                        Icon(Icons.arrow_forward_ios),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
                padding: const EdgeInsets.only(
                    top: 10, bottom: 10, left: 10, right: 5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.grey.withOpacity(0.1)),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(Icons.star_rate),
                        SizedBox(
                          width: 20,
                        ),
                        Text('Rete Us',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                      ],
                    ),
                    Row(
                      children: [
                        Icon(Icons.arrow_forward_ios),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
                padding: const EdgeInsets.only(
                    top: 10, bottom: 10, left: 10, right: 5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.grey.withOpacity(0.1)),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(Icons.share),
                        SizedBox(
                          width: 20,
                        ),
                        Text('Share Us',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                      ],
                    ),
                    Row(
                      children: [
                        Icon(Icons.arrow_forward_ios),
                      ],
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: values.userModel != null &&
                        values.userModel!.role == "Admin"
                    ? true
                    : false,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  padding: const EdgeInsets.only(
                      top: 10, bottom: 10, left: 10, right: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.grey.withOpacity(0.1)),
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.roundabout_left),
                          SizedBox(
                            width: 20,
                          ),
                          Text('Change Role',
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold)),
                        ],
                      ),
                      Row(
                        children: [
                          Icon(Icons.arrow_forward_ios),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const PolicyScreen()));
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  padding: const EdgeInsets.only(
                      top: 10, bottom: 10, left: 10, right: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.grey.withOpacity(0.1)),
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.privacy_tip_outlined),
                          SizedBox(
                            width: 20,
                          ),
                          Text('Privacy Policy',
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold)),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [Icon(Icons.arrow_forward_ios)],
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
                padding: const EdgeInsets.only(
                    top: 10, bottom: 10, left: 10, right: 5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.grey.withOpacity(0.1)),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(Icons.support),
                        SizedBox(
                          width: 20,
                        ),
                        Text('Support',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                      ],
                    ),
                    Row(
                      children: [
                        Icon(Icons.arrow_forward_ios),
                      ],
                    ),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  Dialogs.bottomMaterialDialog(
                      msg: 'Your account will be logout from App!',
                      title: 'Confirm',
                      context: context,
                      lottieBuilder: Lottie.asset(
                        'animation/warning.json',
                        fit: BoxFit.contain,
                      ),
                      actions: [
                        IconsButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          text: 'No',
                          iconData: Icons.cancel_outlined,
                          textStyle: const TextStyle(color: Colors.grey),
                          iconColor: Colors.grey,
                        ),
                        Consumer<LoginProviderController>(
                          builder: (context, value, child) {
                            return IconsButton(
                              onPressed: () async {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return const Center(
                                      child: Center(
                                          child: SpinKitFadingCircle(
                                        color: Colors.white,
                                        size: 35.0,
                                      )),
                                    );
                                  },
                                );
                                await Future.delayed(
                                    const Duration(seconds: 2));
                                if (!context.mounted) return;
                                Navigator.of(context).pop();

                                value.authLogOut();
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const RegisterApp()));
                              },
                              text: 'Yes',
                              iconData: Icons.delete,
                              color: Colors.red,
                              textStyle: const TextStyle(color: Colors.white),
                              iconColor: Colors.white,
                            );
                          },
                        ),
                      ]);
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  padding: const EdgeInsets.only(
                      top: 10, bottom: 10, left: 10, right: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.grey.withOpacity(0.1)),
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.logout, color: Colors.red),
                          SizedBox(
                            width: 20,
                          ),
                          Text('Logout',
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold)),
                        ],
                      ),
                      Row(
                        children: [
                          Icon(Icons.arrow_forward_ios),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              )
            ]),
          ),
        );
      },
    );
  }
}

// Future<void> _dialogBuilder(BuildContext context) {
//   return showDialog<void>(
//     context: context,
//     builder: (BuildContext context) {
//       return AlertDialog(
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.circular(5.0),
//         ),
//         title: const Row(
//           children: [
//             Text('Log out'),
//             SizedBox(
//               width: 5,
//             ),
//             Icon(
//               Icons.warning_amber,
//               color: Colors.red,
//             )
//           ],
//         ),
//         content: const Text(
//           'Your account will be logout from App!',
//         ),
//         actions: <Widget>[
//           TextButton(
//             style: TextButton.styleFrom(
//                 textStyle: Theme.of(context).textTheme.labelLarge,
//                 backgroundColor: Colors.red),
//             child: const Text('Cancel',
//                 style: TextStyle(
//                     color: Colors.white, fontWeight: FontWeight.bold)),
//             onPressed: () {
//               Navigator.of(context).pop();
//             },
//           ),
//           Consumer<LoginProviderController>(
//             builder: (context, value, child) {
//               return TextButton(
//                 style: TextButton.styleFrom(
//                     textStyle: Theme.of(context).textTheme.labelLarge,
//                     backgroundColor: Colors.blue.shade700),
//                 child: const Text('Okay',
//                     style: TextStyle(
//                         color: Colors.white, fontWeight: FontWeight.bold)),
//                 onPressed: () async {
//                   showDialog(
//                     context: context,
//                     builder: (context) {
//                       return const Center(
//                         child: Center(
//                             child: SpinKitFadingCircle(
//                           color: Colors.black,
//                           size: 35.0,
//                         )),
//                       );
//                     },
//                   );
//                   await Future.delayed(const Duration(seconds: 2));
//                   if (!context.mounted) return;
//                   Navigator.of(context).pop();
//
//                   value.authLogOut();
//                   Navigator.push(
//                       context,
//                       MaterialPageRoute(
//                           builder: (context) => const RegisterApp()));
//                 },
//               );
//             },
//           ),
//         ],
//       );
//     },
//   );
// }
