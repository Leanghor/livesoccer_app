import 'package:flutter/material.dart';

class NewsScreen extends StatelessWidget {
  const NewsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () {
          return Future<void>.delayed(const Duration(seconds: 2));
        },
        child: SingleChildScrollView(
          child: Container(
              margin: const EdgeInsets.only(top: 50, left: 10, right: 10),
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('Transfers',
                      style: TextStyle(color: Colors.red, fontSize: 18)),
                  Image.network(
                    'https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/bltc4dfd0e8df21d43a/653d361ec38ac8040a9626f4/GOAL_-_Blank_WEB_-_Facebook_(9).jpg?auto=webp&format=pjpg&width=3840&quality=60',
                    width: double.infinity,
                    height: 200,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  const Text(
                      "We've had the era of Lionel Messi and Cristiano Ronaldo, but it now feels like the Englishman is about to dominate the biggest fixture in football"),
                  GridView.builder(
                    padding: const EdgeInsets.only(top: 20),
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: 10,
                    itemBuilder: (context, index) {
                      return Container(
                        decoration: const BoxDecoration(),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('News',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 14)),
                            Image.network(
                              'https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/bltc4dfd0e8df21d43a/653d361ec38ac8040a9626f4/GOAL_-_Blank_WEB_-_Facebook_(9).jpg?auto=webp&format=pjpg&width=3840&quality=60',
                              width: double.infinity,
                              height: 100,
                              fit: BoxFit.cover,
                            ),
                            const Text(
                                "We've had the era of Lionel Messi and Cristiano Ronaldo, but it now feels like the Englishman is about to dominate the biggest fixture in football",
                                overflow: TextOverflow.ellipsis,
                                maxLines: 4,
                                style: TextStyle(
                                  fontSize: 10,
                                )),
                          ],
                        ),
                      );
                    },
                    gridDelegate:
                        const SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 200.0,
                      mainAxisSpacing: 10.0,
                      crossAxisSpacing: 10.0,
                      childAspectRatio: 1.0,
                    ),
                  )
                ],
              )),
        ),
      ),
    );
  }
}
