import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_demo3/globle/globle_key.dart';
import 'package:flutter_demo3/provider/login_provider.dart';
import 'package:flutter_demo3/ui/area_screen/cambodain_screen.dart';
import 'package:flutter_demo3/ui/area_screen/europe_screen.dart';
import 'package:flutter_demo3/ui/message_screen.dart';
import 'package:flutter_demo3/ui/notification_screen.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  String? token = "";
  bool isChange = true;
  bool? isLoading;
  @override
  void initState() {
    super.initState();
    setState(() {
      token = storeToken.$;
    });
    if(token != null){
      Provider.of<LoginProviderController>(context, listen: false).getUserInformation();
    }else{
      print("I don't see you, please go away.");
    }
    print("Home $token");
  }

  void changeMenu() async {
    setState(() {
      isLoading = true;
    });
    await Future.delayed(const Duration(seconds: 1));
    setState(() {
      isLoading = false;
      isChange = !isChange;
    });
  }

  int currentPage = 0;
  var zoom = [
    const EuropeScreen(),
    const CambodiaScreen(),
  ];

  void _onItemTapped(int index) {
    currentPage = index;
    switch (index) {
      case 0:
        const EuropeScreen();
        break;
      case 1:
        const CambodiaScreen();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final value = Provider.of<LoginProviderController>(context);
    return RefreshIndicator(
      onRefresh: () async {
        return Future<void>.delayed(const Duration(seconds: 2));
      },
      child: Scaffold(
          key: scaffoldKey,
          body: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  ClipPath(
                    clipper: OvalBottomBorderClipper(),
                    child: Container(
                        height: 300,
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        width: double.maxFinite,
                        decoration: const BoxDecoration(
                          color: Colors.blue,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/staduim.jpg"))),
                      child: Column(
                          children: [
                            const SizedBox(
                              height: 50,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    const Icon(Icons.menu, color: Colors.white),
                                    isLoading == true
                                      ? const SizedBox(
                                        width: 20,
                                        height: 20,
                                        child: SpinKitFadingCircle(
                                          color: Colors.white,
                                          size: 15.0,
                                        )
                                      )
                                      : isChange
                                        ? Image.asset(
                                            'assets/icons/european-union.png',
                                            width: 20,
                                            height: 20,
                                            fit: BoxFit.cover,
                                        )
                                        : Image.asset(
                                            'assets/icons/cambodia.png',
                                            width: 20,
                                            height: 20,
                                            fit: BoxFit.cover,
                                        ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    InkWell(
                                      borderRadius: BorderRadius.circular(50),
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => const NotificationScreen()
                                          ),
                                        );
                                      },
                                      child: Stack(children: [
                                        Container(
                                          alignment: Alignment.center,
                                          width: 30,
                                          height: 30,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(50)),
                                          child: const Icon(
                                              Icons.notifications_active_sharp,
                                              color: Colors.blue),
                                        ),
                                        if(value.userModel != null && value.userModel!.messages!.isNotEmpty
                                            && value.userModel!.messages!.toString() != "")...[
                                          Positioned(
                                          right: 3,
                                          top: 0,
                                          child: Container(
                                            width: 13,
                                            height: 13,
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(50),
                                              color: Colors.red,
                                            ),
                                            child: Text(
                                              value.userModel!.messages!.length.toString(),
                                              style: const TextStyle(
                                                color: Colors.white,
                                                fontSize: 7,
                                              ),)
                                            )
                                        )
                                        ],
                                        ]
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                              const MessageScreen()
                                          )
                                        );
                                      },
                                      child: const Icon(Icons.message,
                                          color: Colors.redAccent),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            Container(
                              margin:
                                  const EdgeInsets.only(top: 50, left: 5, right: 5),
                              padding: const EdgeInsets.all(5),
                              width: MediaQuery.of(context).size.width,
                              height: 50,
                              decoration: BoxDecoration(
                                  color: Colors.lightGreen.withOpacity(0.7),
                                  borderRadius: BorderRadius.circular(40)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                      width: 150,
                                      decoration: BoxDecoration(
                                          color: Colors.blue.withOpacity(
                                              isChange == false ? 0.7 : 0.0),
                                          borderRadius: BorderRadius.circular(40)),
                                      child: InkWell(
                                        onTap: () async {
                                          changeMenu();
                                          await Future.delayed(
                                              const Duration(seconds: 1), () {});
                                          _onItemTapped(1);
                                        },
                                        child: Center(
                                          child: Text('Cambodia',
                                              style: GoogleFonts.aboreto(
                                                textStyle: const TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold),
                                              )),
                                        ),
                                      )),
                                  Container(
                                      width: 150,
                                      decoration: BoxDecoration(
                                          color: Colors.blue.withOpacity(
                                              isChange == false ? 0.0 : 0.7),
                                          borderRadius: BorderRadius.circular(40)),
                                      child: InkWell(
                                        onTap: () async {
                                          changeMenu();
                                          await Future.delayed(
                                              const Duration(seconds: 1), () {});
                                          _onItemTapped(0);
                                        },
                                        child: Center(
                                            child: Text('Europe',
                                                style: GoogleFonts.aboreto(
                                                  textStyle: const TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 16,
                                                      fontWeight: FontWeight.bold),
                                                )
                                              )
                                            ),
                                      )
                                      ),
                                ],
                              ),
                            )
                          ],
                        )),
                  ),
                  zoom.elementAt(currentPage),
            ],
          ))),
    );
  }
}


