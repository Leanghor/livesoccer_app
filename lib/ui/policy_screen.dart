import 'package:flutter/material.dart';
import 'package:flutter_demo3/provider/login_provider.dart';
import 'package:lottie/lottie.dart';
import 'package:material_dialogs/material_dialogs.dart';
import 'package:material_dialogs/widgets/buttons/icon_button.dart';
import 'package:provider/provider.dart';

class PolicyScreen extends StatefulWidget {
  const PolicyScreen({super.key});

  @override
  State<PolicyScreen> createState() => _PolicyScreenState();
}

class _PolicyScreenState extends State<PolicyScreen> {
  final TextEditingController messageController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool? validate = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<LoginProviderController>(context);
    return RefreshIndicator(
        child: Scaffold(
          body: CustomScrollView(
            slivers: [
              SliverAppBar(
                pinned: true,
                automaticallyImplyLeading: false,
                flexibleSpace: FlexibleSpaceBar(
                  background: Container(
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Colors.blue,
                          Colors.green
                        ], // Set your gradient colors
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                    ),
                  ),
                ),
                title: const Text("Privacy Policy for Soccer App",
                  style: TextStyle(
                    fontSize: 16,
                    decoration: TextDecoration.underline,
                    decorationColor: Colors.white,
                    color: Colors.white,
                    fontWeight: FontWeight.bold)),
                leading: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: const Icon(
                      Icons.arrow_back_ios,
                      size: 16,
                      color: Colors.white,
                    )),
              ),
              const SliverPadding(
                padding:
                    EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                sliver: SliverToBoxAdapter(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text('Effective Date: ',
                              style: TextStyle(
                                  decoration: TextDecoration.underline)),
                          Text('30-December-2030'),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 15, bottom: 15),
                        child: Text(
                          "1. Overview",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                          "Thank you for using Soccer App. This Privacy Policy is designed to help you understand how we collect, use, and safeguard your personal information. By using the App, you agree to the terms outlined in this Privacy Policy."),
                      Padding(
                        padding: EdgeInsets.only(top: 15, bottom: 15),
                        child: Text(
                          "2. Information We Collect",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                          "2.1. Personal Information: We may collect personal information such as your name, email address, and other relevant details when you register or interact with the App."),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                          "2.2. Usage Information: We gather information about how you use the App, including but not limited to, the features you access, the content you view, and your interactions with other users."),
                      Padding(
                        padding: EdgeInsets.only(top: 15, bottom: 15),
                        child: Text(
                          "3. How We Use Your Information",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                          "3.1. Providing Services: We use the collected information to provide and improve our services, customize your experience, and respond to your inquiries."),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                          "3.2. Communication: We may use your contact information to send you updates, newsletters, or promotional materials related to the App. You can opt-out of these communications at any time."),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                          "3.3. Analytics: We utilize analytics tools to understand how users interact with the App and to improve its functionality and user experience."),
                      Padding(
                        padding: EdgeInsets.only(top: 15, bottom: 15),
                        child: Text(
                          "4. Sharing Your Information",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                          "4.1. Third-Party Services: We may share your information with third-party service providers who assist us in delivering and improving our services."),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                          "4.2. Legal Compliance: We may disclose your information if required by law, court order, or other legal processes."),
                      SizedBox(
                        height: 5,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 15, bottom: 15),
                        child: Text(
                          "5. Security",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                          "We implement reasonable security measures to protect the confidentiality and integrity of your personal information. However, no method of transmission over the internet or electronic storage is 100% secure, and we cannot guarantee absolute security."),
                      SizedBox(
                        height: 5,
                      ),
                    ],
                  ),
                ),
              ),
              SliverPadding(
                padding: const EdgeInsets.only(bottom: 10),
                sliver: SliverToBoxAdapter(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          onPressed: () async {
                            provider.clearMessage();
                            setState(() {
                              validate =! validate!;
                            });

                            showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  backgroundColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  title: const Text('Feed Back.',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                          fontSize: 16)),
                                  content: SizedBox(
                                    width: 300,
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Form(
                                          key: _formKey,
                                          child: TextFormField(
                                            style: const TextStyle(
                                                color: Colors.black
                                            ),
                                            controller: provider.messageController,
                                            decoration: const InputDecoration(
                                              hintText: 'Write your message here...',
                                            ),
                                            validator: (value) {
                                              if (value == null || value.isEmpty) {
                                                return 'Please enter a message';
                                              }
                                              return null;
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  actions: [
                                    IconsButton(
                                      onPressed: () async {
                                        if (_formKey.currentState!.validate()) {
                                          await provider.sendMessageFromUser(
                                              content: provider
                                                  .messageController.text
                                                  .toString());
                                          if (!context.mounted) return;
                                          Navigator.pop(context);
                                        } else {
                                          setState(() {
                                            validate =! validate!;
                                          });
                                        }
                                      },
                                      text: 'Send',
                                      iconData: Icons.send,
                                      color: Colors.teal,
                                      textStyle:
                                          const TextStyle(color: Colors.white),
                                      iconColor: Colors.white,
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                          style: ElevatedButton.styleFrom(
                              shape: ContinuousRectangleBorder(
                                  borderRadius: BorderRadius.circular(7)),
                              backgroundColor: Colors.red),
                          child: const Text(
                            'Disagree',
                            style: TextStyle(color: Colors.white),
                          )),
                      const SizedBox(
                        width: 10,
                      ),
                      ElevatedButton(
                          onPressed: () async {
                            showDialogMessageAgree(context);
                          },
                          style: ElevatedButton.styleFrom(
                              shape: ContinuousRectangleBorder(
                                  borderRadius: BorderRadius.circular(7)),
                              backgroundColor: Colors.blue),
                          child: const Text(
                            'Agree',
                            style: TextStyle(color: Colors.white),
                          ))
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        onRefresh: () {
          return Future.delayed(const Duration(seconds: 1));
        });
  }

  Future<void> showDialogMessageAgree(context) async {
    Dialogs.materialDialog(
        msg: 'Thank for your agree.',
        title: "Thank You!",
        lottieBuilder: Lottie.asset(
          'animation/agree.json',
          fit: BoxFit.contain,
        ),
        color: Colors.white,
        context: context,
        actions: [
          IconsButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            text: 'Cancel',
            color: Colors.red,
            textStyle: const TextStyle(
                color: Colors.white,
                decoration: TextDecoration.underline,
                decorationColor: Colors.white),
            iconColor: Colors.white,
            iconData: Icons.cancel_outlined,
          ),
          IconsButton(
            onPressed: () {
              Navigator.pop(context);
              Navigator.pop(context);
            },
            text: 'Confirm',
            color: Colors.teal,
            textStyle: const TextStyle(
                color: Colors.white,
                decoration: TextDecoration.underline,
                decorationColor: Colors.white),
            iconColor: Colors.white,
            iconData: Icons.done,
          ),
        ]);
  }
}
