import 'package:shared_value/shared_value.dart';
String userIdString = "";

final SharedValue<String> userName = SharedValue(
    value: "",
    key: "user_name"
);

SharedValue<String> userId = SharedValue(value: "", key: "id");

final SharedValue<String> userEmail = SharedValue(
    value: '',
    key: "email"
);

final SharedValue<String> userPassword = SharedValue(
    value: "",
    key: "password"
);

final SharedValue<String?> imageProfile = SharedValue(
  value: null,
  key: 'image_profile',
);

final SharedValue<String?> storeToken = SharedValue(
  value: "",
  key: "access_token",
);

final SharedValue<String?> url = SharedValue(
  value: "",
  key: "baseUrl",
);
