import 'package:flutter/foundation.dart';
import 'package:flutter_demo3/master_data/master_data_fixed.dart';
import 'package:flutter_demo3/models/master_premier_model.dart';

class MasterDataRepo{
  List<MasterModel> masterBizModelList({required String nameClub}) {
    var leagueData = MasterDataBasUrl();
    var data = leagueData.league[nameClub];
    List<MasterModel> resultList = [];
    for (var item in data!) {
      resultList.add(MasterModel.fromJson(item));
    }
    if (kDebugMode) {
      print("resultList : $resultList");
      print("data : $data");
    }
    return resultList;
  }
}