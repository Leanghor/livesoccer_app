import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_demo3/base_url/globle_uri.dart';
import 'package:flutter_demo3/globle/globle_key.dart';
import 'package:flutter_demo3/models/profile_model.dart';
import 'package:http/http.dart' as http;

class ProfileRepository {
  Future<List<ProfileModel>?> fetchUserProfile() async {
    List<ProfileModel> profile = [];
    try {
      var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${storeToken.$}',
      };
      var request =
          http.Request('GET', Uri.parse('$baseUri/api/auth/user-profile'));
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var data = (await response.stream.bytesToString());
        var json = jsonDecode(data);
        for (var i in json['profile_user']) {
          profile.add(ProfileModel.fromJson(i));
        }
        if (kDebugMode) {
          print("jjjjjjjj $profile");
        }
        return profile;
      } else {
        throw Exception('Failed to load user profile');
      }
    } catch (e) {
      if (kDebugMode) {
        print("error $e");
      }
    }
    return null;
  }
}
