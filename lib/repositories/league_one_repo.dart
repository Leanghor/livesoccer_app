import 'dart:convert';
import 'package:flutter_demo3/models/league_one_model.dart';
import 'package:http/http.dart' as http;

import '../base_url/globle_uri.dart';

class OneRepository {
  Future<List<LeagueOneModel>> callApi() async {
    List<LeagueOneModel> lOne = [];
    final res = await http.get(Uri.parse('$baseUri/api/league1/fetch_data'));

    if(res.statusCode == 200){
      var json = jsonDecode(res.body);
      for(var result in json) {
        lOne.add(LeagueOneModel.fromJson(result));
      }
      return lOne;
    }else{
      print("error");
    }
    return [];
  }
}