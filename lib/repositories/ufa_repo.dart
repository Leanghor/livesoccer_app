import 'dart:convert';
import 'package:http/http.dart' as http;
import '../base_url/globle_uri.dart';
import '../models/ufa_model.dart';

class EuFaRepository {
  Future<List<EuFaModel>> getData() async {
    List<EuFaModel> d = [];
    final response = await http.get(
      Uri.parse('$baseUri/api/products/fetchMatch'),
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      for(var result in jsonResponse){
        d.add(EuFaModel.fromJson(result));
      }
      return d;
    } else {
      throw Exception('Unexpected error occured!');
    }
  }
}

