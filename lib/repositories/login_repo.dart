import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter_demo3/globle/globle_key.dart';
import 'package:flutter_demo3/models/user_model.dart';
import 'package:http/http.dart' as http;
import '../base_url/globle_uri.dart';

class LoginRepository {
  int? status;

  Future<dynamic> userLogin(
      {required String email, required String password}) async {
    var headers = {'Accept': 'application/json'};
    var request =
        http.MultipartRequest('POST', Uri.parse('$baseUri/api/auth/login'));
    request.fields.addAll({'email': email.trim(), 'password': password.trim()});
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      var data = (await response.stream.bytesToString());
      var json = jsonDecode(data);
      return json;
    } else {
      if (kDebugMode) {
        print("status => ${response.statusCode}");
      }
      return null;
    }
  }

  Future<UserModel?> fetchUser() async {
    try {
      var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${storeToken.$}',
      };
      var url = Uri.parse("$baseUri/api/auth/user-profile");
      var user = await http.get(url, headers: headers);
      if (user.statusCode == 200) {
        var json = jsonDecode(user.body);
        return UserModel.fromJson(json);
      }
      debugPrint("Unexpected status code: ${user.statusCode}");
      return null;
    } catch (e) {
      debugPrint("Error fetching user: $e");
      return null;
    }
  }


  Future<dynamic> submitMessage({required String notification}) async {
    var headers = {'Content-Type': 'application/json'};
    var request = http.Request(
        'POST', Uri.parse('$baseUri/api/soccer/store'));
    request.body = json.encode({"user_id": userId.$, "notification": notification});
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      var data = (await response.stream.bytesToString());
      var json = jsonDecode(data);
      return json;
    } else {
      if (kDebugMode) {
        print(response.reasonPhrase);
      }
    }
  }

  Future<dynamic> deleteDataRepo(String id) async {
    try {
      final url = Uri.parse('$baseUri/api/soccer/remove/$id');
      final response = await http.delete(url);
      if (response.statusCode == 200) {
        status = 200;
      } else {
        if (kDebugMode) {
          print('Failed to delete data. Status code: ${response.statusCode}');
        }
      }
    } catch (error) {
      if (kDebugMode) {
        print('Error deleting data: $error');
      }
    }
  }

  Future<dynamic> updateNameANDEmail(String id, {required String name, required String role}) async{
    var headers = {
      'Authorization': 'Bearer ${storeToken.$}',
      'Content-Type': 'application/json'
    };
    var request = http.Request('POST', Uri.parse('$baseUri/api/auth/user/update/$id'));
    request.body = json.encode({
      "name": name,
      "role": role
    });
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      var data = await response.stream.bytesToString();
      var json = jsonDecode(data);
      return json;
    }
    else {
      debugPrint(response.reasonPhrase);
    }
  }

  Future<dynamic> updateProfile(String id, {required String imageFile}) async {
    var headers = {
      'Authorization': 'Bearer ${storeToken.$}',
      'Content-Type': 'application/json'
    };
    var request = http.MultipartRequest('POST', Uri.parse('$baseUri/api/auth/user/update-profile-image/$id'));
    request.files.add(await http.MultipartFile.fromPath('profile_image', imageFile));
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();
    debugPrint(await response.stream.bytesToString());
    if (response.statusCode == 200) {
      var data = await response.stream.bytesToString();
      var json = jsonDecode(data);
      return json;
    }
    else {
      debugPrint(response.reasonPhrase);
    }
  }

  Future<dynamic> removeUserImage({required String id}) async {
    var headers = {
      'Content-Type': 'application/json',
    };
    var request = http.Request('DELETE', Uri.parse('$baseUri/api/auth/user/remove-file-image/$id'));

    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      debugPrint(await response.stream.bytesToString());
      debugPrint("++++++++ ${response.statusCode}");
    }
    else {
      debugPrint(response.reasonPhrase);
    }

  }
}


