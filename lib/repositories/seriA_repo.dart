import 'dart:convert';
import 'package:flutter_demo3/models/seriA_model.dart';
import 'package:http/http.dart' as http;
import '../base_url/globle_uri.dart';

class SeriARepository {
  Future<List<SeriAModel>> seriA() async {
    List<SeriAModel> tempSeri = [];
    final response = await http.get(Uri.parse('$baseUri/api/seri_A/fetch_data'));
    if(response.statusCode == 200){
      var jsonData = await jsonDecode(response.body);
      for(var result in jsonData){
        tempSeri.add(SeriAModel.fromJson(result));
      }
      return tempSeri;
    }else{
      print('error');
    }


    return [];
  }
}