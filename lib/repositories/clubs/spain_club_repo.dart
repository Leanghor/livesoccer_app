import 'dart:convert';

import 'package:flutter_demo3/base_url/globle_uri.dart';

import '../../models/club_name/spain_club.dart';
import 'package:http/http.dart' as http;

class ClubSpainRepo{
  Future<List<ClubSapinModel>> nameClub() async {
    List<ClubSapinModel> c = [];
    
    final response = await http.get(Uri.parse('$baseUri/api/sapin_club/fetch_data'));
    if(response.statusCode == 200){
      var json = jsonDecode(response.body);
      // print("kkkkkkkk $json");
      for(var result in json){
        c.add(ClubSapinModel.fromJson(result));
      }
      return c;
    }
    return [];
  }
}