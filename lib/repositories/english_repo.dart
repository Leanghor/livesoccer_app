import 'dart:convert';
import 'package:flutter_demo3/models/english_model.dart';
import 'package:http/http.dart' as http;
import '../base_url/globle_uri.dart';

class PremierLeagueRepository{
  Future<List<PremierLeagueModel>> efetch() async {
    List<PremierLeagueModel> eGet = [];

    final response = await http.get(
      Uri.parse('$baseUri/api/english_premier/all')
    );
    if(response.statusCode == 200){
      var json = await jsonDecode(response.body);
      for(var result in json){
        eGet.add(PremierLeagueModel.fromJson(result));
      }
      return eGet;
    }else{
      print('error + ${response.statusCode}');
    }

    return [];
  }
}