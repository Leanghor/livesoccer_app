import 'dart:convert';
import 'package:http/http.dart' as http;
import '../base_url/globle_uri.dart';
import '../models/laliga_model.dart';

class LaligaRepository {
  Future<List<LaligaModel>> fetchDataFromLaliga() async {
    List<LaligaModel> laliga = [];
    final response = await http.get(
      Uri.parse('$baseUri/api/spain_league/fetch_data'),
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      for(var result in jsonResponse){
        laliga.add(LaligaModel.fromJson(result));
      }
      return laliga;
    } else {
      throw Exception('Unexpected error occured!');
    }
  }
}

