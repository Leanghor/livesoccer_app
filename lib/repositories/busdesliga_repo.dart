import 'dart:convert';
import '../base_url/globle_uri.dart';
import '../models/besdesliga_model.dart';
import 'package:http/http.dart' as http;

class BusdesligaRepository{
  Future<List<BundesligaModel>> bunData() async {
    List<BundesligaModel> _b = [];
    final response = await http.get(
      Uri.parse('$baseUri/api/bundesliga/bundesliga/fetch')
    );
    if(response.statusCode == 200){
      var json = await jsonDecode(response.body);
      for(var result in json){
        _b.add(BundesligaModel.fromJson(result));
      }
      return _b;
    }
    
    return [];
  }
}