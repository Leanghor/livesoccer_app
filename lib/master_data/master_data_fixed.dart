class MasterDataBasUrl{
  var league = {
    "premier_league": [
      {
        "id":"1",
        "name_club": "Arsenal",
        "session": "2023/2024",
        "image":
        "https://www.premierleague.com/resources/rebrand/v7.134.0/i/elements/pl-main-logo.png",
        "lost": [
          {
            "key":"0"
          },
        ],
        "won": [
          {"key": "0"},
          {"key": "1"},
          {"key": "2"},
          {"key": "3"},
          {"key": "4"},
          {"key": "5"},
        ],
        "drawn": [
          {
            "key":"0"
          },
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t3.png",
            "cover":"https://i.pinimg.com/564x/58/71/33/5871338ff523f3fd4039d2f1c6a136b0.jpg",
            "mp": "15",
            "won": "11",
            "drawn": "1",
            "lost": "1",
            "gf": "33",
            "ga": "14",
            "gd": "19",
            "pt": "36"
          }
        ]
      },
      {
        "id":"2",
        "name_club": "Liverpool FC",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t14.png",
        "lost": [
          {"key":"0"},
        ],
        "won": [
          {"key": "0"},
          {"key": "1"},
          {"key": "2"},
          {"key": "3"},
        ],
        "drawn": [
          {"key":"0"},
          {"key":"1"},
          {"key":"2"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t14.png",
            "cover":"https://i.pinimg.com/564x/37/84/99/378499e252464af3a2e5fa93b9be93fb.jpg",
            "mp": "15",
            "won": "10",
            "drawn": "4",
            "lost": "1",
            "gf": "34",
            "ga": "14",
            "gd": "20",
            "pt": "34"
          }
        ]
      },
      {
        "id":"3",
        "name_club": "Manchester City",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t43.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
        ],
        "won": [
          {"key": "0"},
          {"key": "1"},
          {"key": "2"},
        ],
        "drawn": [
          {"key":"0"},
          {"key":"1"},
          {"key":"2"},
          {"key":"3"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t43.png",
            "cover":"https://i.pinimg.com/564x/cb/f2/54/cbf2542f7a06b99914957e6d95dc106d.jpg",
            "mp": "15",
            "won": "9",
            "drawn": "3",
            "lost": "3",
            "gf": "36",
            "ga": "17",
            "gd": "19",
            "pt": "30"
          }
        ]
      },
      {
        "id":"4",
        "name_club": "Tottenham Hotspur",
        "session": "2023/2024",
        "image":
        "https://www.premierleague.com/resources/rebrand/v7.134.0/i/elements/pl-main-logo.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
          {"key":"2"},
          {"key":"3"},
          {"key":"4"},
        ],
        "won": [
          {"key": "0"},
        ],
        "drawn": [
          {"key":"0"},
          {"key":"1"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t6.png",
            "cover":"https://i.pinimg.com/564x/07/9c/10/079c10fc59419dff9b68ac0283036e77.jpg",
            "mp": "15",
            "won": "8",
            "drawn": "3",
            "lost": "4",
            "gf": "29",
            "ga": "22",
            "gd": "7",
            "pt": "27"
          }
        ]
      },
      {
        "id":"5",
        "name_club": "Aston Villa",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t7.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
        ],
        "won": [
          {"key": "0"},
          {"key": "1"},
          {"key": "2"},
          {"key": "3"},
        ],
        "drawn": [
          {"key":"0"},
          {"key":"1"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t7.png",
            "cover":"https://i.pinimg.com/564x/d9/54/a6/d954a6f39874940647f7ac53b031463d.jpg",
            "mp": "15",
            "won": "10",
            "drawn": "2",
            "lost": "3",
            "gf": "34",
            "ga": "20",
            "gd": "14",
            "pt": "32"
          }
        ]
      },
      {
        "id":"6",
        "name_club": "Manchester United",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t7.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
        ],
        "won": [
          {"key": "0"},
          {"key": "1"},
          {"key": "2"},
          {"key": "3"},
          {"key": "4"},
        ],
        "drawn": [
          {"key":"0"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t1.png",
            "cover":"https://i.pinimg.com/564x/30/a6/0f/30a60fd2da3a488dfc1ee50ffd4f33eb.jpg",
            "mp": "15",
            "won": "9",
            "drawn": "0",
            "lost": "6",
            "gf": "18",
            "ga": "18",
            "gd": "0",
            "pt": "27"
          }
        ]
      },
      {
        "id":"7",
        "name_club": "Newcastle United",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t7.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
          {"key":"2"},
          {"key":"3"},
        ],
        "won": [
          {"key": "0"},
          {"key": "1"},
          {"key": "2"},
        ],
        "drawn": [
          {"key":"0"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t4.png",
            "cover":"https://i.pinimg.com/564x/cc/bb/c5/ccbbc5ece0eeb9e44fa1a80e3d8ff2ee.jpg",
            "mp": "16",
            "won": "8",
            "drawn": "2",
            "lost": "6",
            "gf": "33",
            "ga": "21",
            "gd": "12",
            "pt": "26"
          }
        ]
      },
      {
        "id":"8",
        "name_club": "Brighton and Hove Albion",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t7.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
        ],
        "won": [
          {"key": "0"},
          {"key": "1"},
          {"key": "2"},
        ],
        "drawn": [
          {"key":"0"},
          {"key":"1"},
          {"key":"2"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t36.png",
            "cover":"https://i.pinimg.com/564x/eb/a0/49/eba0498f1bfe4765fcab9be3d3507ff5.jpg",
            "mp": "16",
            "won": "7",
            "drawn": "5",
            "lost": "4",
            "gf": "33",
            "ga": "28",
            "gd": "5",
            "pt": "26"
          }
        ]
      },
      {
        "id":"9",
        "name_club": "Wast Ham United",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t7.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
        ],
        "won": [
          {"key": "0"},
          {"key": "1"},
          {"key": "2"},
          {"key": "3"},
        ],
        "drawn": [
          {"key":"0"},
          {"key":"1"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t21.png",
            "cover":"https://i.pinimg.com/564x/b8/4d/bd/b84dbd7e8942050a8ea4af77dda3458c.jpg",
            "mp": "16",
            "won": "7",
            "drawn": "3",
            "lost": "6",
            "gf": "26",
            "ga": "30",
            "gd": "-4",
            "pt": "24"
          }
        ]
      },
      {
        "id":"10",
        "name_club": "Fulham",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t7.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
          {"key":"2"},
        ],
        "won": [
          {"key": "0"},
          {"key": "1"},
          {"key": "2"},
          {"key": "3"},
        ],
        "drawn": [
          {"key":"0"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t54.png",
            "cover":"https://i.pinimg.com/564x/cd/0d/e0/cd0de0e04d4553ce85436563f68860ef.jpg",
            "mp": "16",
            "won": "6",
            "drawn": "3",
            "lost": "7",
            "gf": "26",
            "ga": "26",
            "gd": "0",
            "pt": "21"
          }
        ]
      },
      {
        "id":"11",
        "name_club": "Brentford",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t7.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
          {"key":"2"},
        ],
        "won": [
          {"key": "0"},
          {"key": "1"},
          {"key": "2"},
          {"key": "3"},
        ],
        "drawn": [
          {"key":"0"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t94.png",
            "cover":"https://i.pinimg.com/564x/96/08/ff/9608ff98019ae45b621d997754ec5a06.jpg",
            "mp": "16",
            "won": "5",
            "drawn": "4",
            "lost": "7",
            "gf": "23",
            "ga": "22",
            "gd": "1",
            "pt": "19"
          }
        ]
      },
      {
        "id":"12",
        "name_club": "Chelsea",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t7.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
          {"key":"2"},
          {"key":"3"},
        ],
        "won": [
          {"key": "0"},
          {"key": "1"},
        ],
        "drawn": [
          {"key":"0"},
          {"key":"1"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t8.png",
            "cover":"https://i.pinimg.com/564x/d3/37/67/d33767212461d1faf7eeb9243a928a95.jpg",
            "mp": "16",
            "won": "5",
            "drawn": "4",
            "lost": "7",
            "gf": "26",
            "ga": "26",
            "gd": "10",
            "pt": "19"
          }
        ]
      },
      {
        "id":"13",
        "name_club": "Wolverhampton Wanderers",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t7.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
          {"key":"2"},
        ],
        "won": [
          {"key": "0"},
          {"key": "1"},
          {"key": "2"},
        ],
        "drawn": [
          {"key":"0"},
          {"key":"1"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t39.png",
            "cover":"https://i.pinimg.com/564x/9a/22/8d/9a228d3f0dcf7f1e5e0e49f36ed8c3ca.jpg",
            "mp": "16",
            "won": "5",
            "drawn": "4",
            "lost": "7",
            "gf": "21",
            "ga": "26",
            "gd": "-5",
            "pt": "19"
          }
        ]
      },
      {
        "id":"14",
        "name_club": "Burnemouth",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t7.png",
        "lost": [
          {"key":"0"},
        ],
        "won": [
          {"key": "0"},
          {"key": "1"},
          {"key": "2"},
          {"key": "3"},
          {"key": "4"},
        ],
        "drawn": [
          {"key":"0"},
          {"key":"1"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t91.png",
            "cover":"https://i.pinimg.com/originals/26/77/de/2677de7de12300cfce37ac09d852d28e.png",
            "mp": "16",
            "won": "5",
            "drawn": "4",
            "lost": "7",
            "gf": "21",
            "ga": "30",
            "gd": "-9",
            "pt": "19"
          }
        ]
      },
      {
        "id":"15",
        "name_club": "Crystal Palace",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t7.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
          {"key":"2"},
          {"key":"3"},
          {"key":"4"},
        ],
        "won": [
          {"key": "0"},
        ],
        "drawn": [
          {"key":"0"},
          {"key":"1"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t31.png",
            "cover":"https://i.pinimg.com/736x/f8/8f/43/f88f4313ae765492b5c06c8f87df3c33.jpg",
            "mp": "16",
            "won": "4",
            "drawn": "4",
            "lost": "8",
            "gf": "15",
            "ga": "23",
            "gd": "-8",
            "pt": "16"
          }
        ]
      },
      {
        "id":"16",
        "name_club": "Nottingham Forest",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t7.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
          {"key":"2"},
          {"key":"3"},
          {"key":"4"},
        ],
        "won": [
          {"key": "0"},
        ],
        "drawn": [
          {"key":"0"},
          {"key":"1"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t17.png",
            "cover":"https://i.pinimg.com/564x/67/b9/9c/67b99c3d3f3968bc320caecd67fdd123.jpg",
            "mp": "16",
            "won": "3",
            "drawn": "5",
            "lost": "8",
            "gf": "17",
            "ga": "28",
            "gd": "-11",
            "pt": "14"
          }
        ]
      },
      {
        "id":"17",
        "name_club": "Everton",
        "session": "2023/2024",
        "image":
        "https://resources.premierleague.com/premierleague/badges/50/t7.png",
        "lost": [
          {"key":"0"},
          {"key":"1"},
        ],
        "won": [
          {"key": "0"},
          {"key":"1"},
          {"key":"2"},
          {"key":"3"},
          {"key":"4"},
        ],
        "drawn": [
          {"key":"0"},
        ],
        "club_information": [
          {
            "logo":
            "https://resources.premierleague.com/premierleague/badges/50/t11.png",
            "cover":"https://i.pinimg.com/564x/56/3f/d6/563fd6aba25be7895937ce93cb31c357.jpg",
            "mp": "16",
            "won": "7",
            "drawn": "2",
            "lost": "7",
            "gf": "20",
            "ga": "20",
            "gd": "0",
            "pt": "13"
          }
        ]
      },
    ],
    'laliga': [
      // {
      //   "name_club": "",
      //   "session": "2023/2024",
      //   "image": "",
      //   "five_last_match": [
      //     {"lost": "", "won": "", "drawn": ""}
      //   ],
      //   "club_information": [
      //     {
      //       "logo": "",
      //       "mp": "",
      //       "won": "",
      //       "drawn": "",
      //       "lost": "",
      //       "gf": "",
      //       "ga": "",
      //       "gd": "",
      //       "pt": ""
      //     }
      //   ]
      // }
    ],
    'ligue_one': [
      // {
      //   "name_club": "",
      //   "session": "2023/2024",
      //   "image": "",
      //   "five_last_match": [
      //     {"lost": "", "won": "", "drawn": ""}
      //   ],
      //   "club_information": [
      //     {
      //       "logo": "",
      //       "mp": "",
      //       "won": "",
      //       "drawn": "",
      //       "lost": "",
      //       "gf": "",
      //       "ga": "",
      //       "gd": "",
      //       "pt": ""
      //     }
      //   ]
      // }
    ],
    'seri_a': [
      // {
      //   "name_club": "",
      //   "session": "2023/2024",
      //   "image": "",
      //   "five_last_match": [
      //     {"lost": "", "won": "", "drawn": ""}
      //   ],
      //   "club_information": [
      //     {
      //       "logo": "",
      //       "mp": "",
      //       "won": "",
      //       "drawn": "",
      //       "lost": "",
      //       "gf": "",
      //       "ga": "",
      //       "gd": "",
      //       "pt": ""
      //     }
      //   ]
      // }
    ],
    'bundesliga': [
      // {
      //   "name_club": "",
      //   "session": "2023/2024",
      //   "image": "",
      //   "five_last_match": [
      //     {"lost": "", "won": "", "drawn": ""}
      //   ],
      //   "club_information": [
      //     {
      //       "logo": "",
      //       "mp": "",
      //       "won": "",
      //       "drawn": "",
      //       "lost": "",
      //       "gf": "",
      //       "ga": "",
      //       "gd": "",
      //       "pt": ""
      //     }
      //   ]
      // }
    ],
  };
}

