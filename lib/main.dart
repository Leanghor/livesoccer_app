import 'package:flutter/material.dart';
import 'package:flutter_demo3/provider/bundesliga_provider.dart';
import 'package:flutter_demo3/provider/club_name/spain_club_provider.dart';
import 'package:flutter_demo3/provider/english_provider.dart';
import 'package:flutter_demo3/provider/laliga_provider.dart';
import 'package:flutter_demo3/provider/league_one_provider.dart';
import 'package:flutter_demo3/provider/login_provider.dart';
import 'package:flutter_demo3/provider/seriA_provider.dart';
import 'package:flutter_demo3/provider/ufa_provider.dart';
import 'package:flutter_demo3/provider/user_infor/profile_provider.dart';
import 'package:flutter_demo3/start_app.dart';
import 'package:provider/provider.dart';
import 'package:shared_value/shared_value.dart';
import 'globle/globle_key.dart';
import 'provider/master_data/master_data_provider.dart';
import 'provider/register_provider.dart';

void main() {
  runApp(
    SharedValue.wrapApp(
      const MyApp()
    )
  );
}
class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  @override

  void initState() {
    init();
    super.initState();
  }

  init() async {
    userName.load();
    userEmail.load();
    userId.load();
    storeToken.load();
    url.load();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => LoginProviderController()),
        ChangeNotifierProvider(create: (context) => SignUpProvider()),
        ChangeNotifierProvider(create: (context) => EuFaProvider()),
        ChangeNotifierProvider(create: (context) => LaligaProvider()),
        ChangeNotifierProvider(create: (context) => LeagueOneProvider()),
        ChangeNotifierProvider(create: (context) => SeriAProvider()),
        ChangeNotifierProvider(create: (context) => PremierLeagueProvider()),
        ChangeNotifierProvider(create: (context) => BundesligaProvider()),
        ChangeNotifierProvider(create: (context) => SapinClubProvider()),
        ChangeNotifierProvider(create: (context) => MasterDataProvider()),
        ChangeNotifierProvider(create: (context) => ProfileControllerProvider()),
      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: StartApp(),
    )
    );
  }
}

