import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class InvalidDataScreen extends StatelessWidget {
  const InvalidDataScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width,
      height: 700,
      child: CachedNetworkImage(
        imageUrl: "https://svec.ae/assets/images/noData.png",
        width: 200,
        height: 200,
        progressIndicatorBuilder: (context, url, downloadProgress) =>
          Container(
            alignment: Alignment.center,
            height: 50,
            width: 50,
            child: CircularProgressIndicator(value: downloadProgress.progress, color: Colors.blue)),
        errorWidget: (context, url, error) => const Icon(Icons.error),
      ),
    );
  }
}
