import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo3/globle/globle_key.dart';
import 'package:flutter_demo3/models/user_model.dart';
import 'package:flutter_demo3/provider/login_provider.dart';
import 'package:flutter_demo3/repositories/login_repo.dart';
import 'package:flutter_demo3/security_screen/register_app.dart';
import 'package:flutter_demo3/ui/home_screen.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_demo3/main_project.dart';
import 'package:flutter_demo3/security_screen/login_app.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StartApp extends StatefulWidget {
  const StartApp({super.key});

  @override
  State<StartApp> createState() => _StartAppState();
}

class _StartAppState extends State<StartApp> {

  @override
  void initState() {
    super.initState();
    clickEven();
  }

  Future<void> clickEven() async {
    showDialog(
      context: context,
      builder: (context) {
        return const SpinKitFadingCircle(
          color: Colors.white,
          size: 40.0,
        );
      },
    );
    await Future.delayed(const Duration(seconds: 1), () async {});
    if (!context.mounted) return;
    Navigator.of(context).pop();

    if(storeToken.$ != "" && storeToken.$!.isNotEmpty) {
      try{
        await LoginRepository().fetchUser().then((value) => {
          debugPrint("======= data $value"),
          debugPrint("======= data ${storeToken.$}"),
          if(value != null){
            Provider.of<LoginProviderController>(listen: false, context).updateUser( value, isUpdate: true),
            Navigator.push(context, MaterialPageRoute(builder: (context) => const MainPage())),
          }else{
            Navigator.push(context, MaterialPageRoute(builder: (context) => const LoginApp(),))
          }
        });
      }catch(e){
        debugPrint("======== $e");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Colors.blue,
                Colors.red,
              ],
            )),
            width: double.maxFinite,
            height: double.maxFinite,
            child: Column(
              children: [
                const SizedBox(height: 100),
                Text('Be_Live',
                  style: GoogleFonts.acme(
                    textStyle: const TextStyle(
                      color: Colors.white,
                      fontSize: 40,
                      fontWeight: FontWeight.bold),
                  )
                ),
                const SizedBox(
                  height: 50,
                ),
                Image.asset(
                  'assets/images/pic1.png',
                  width: 400,
                  height: 400,
                ),
                Container(
                  width: 250,
                  height: 50,
                  margin: const EdgeInsets.only(top: 20),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.green,
                      foregroundColor: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(70)
                      )
                    ),
                    onPressed: () {
                      clickEven();
                    },
                    child: Text('Quick Setup',
                      style: GoogleFonts.acme(
                        textStyle: const TextStyle(
                          fontSize: 16,
                        ),
                      )
                    )
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Text('Are you ready have an account? | '),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const RegisterApp()));
                      },
                      child: const Text('Sign in',
                        style: TextStyle(color: Colors.white)),
                    ),
                  ],
                )
              ],
            )
        )
    );
  }
}


