import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo3/main_project.dart';
import 'package:flutter_demo3/provider/login_provider.dart';
import 'package:flutter_demo3/security_screen/register_app.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class LoginApp extends StatefulWidget {
  const LoginApp({super.key});

  @override
  State<LoginApp> createState() => _LoginAppState();
}

class _LoginAppState extends State<LoginApp> {
  var confirmPasswordController = TextEditingController(text: '');
  bool isPasswordVisible = false;
  bool _checked = false;

  @override
  Widget build(BuildContext context) {
    final loginProviderController =
        Provider.of<LoginProviderController>(context);
    return Scaffold(
      body: Consumer<LoginProviderController>(
        builder: (context, provider, child) {
          return SingleChildScrollView(
            child: Container(
              margin: const EdgeInsets.only(
                left: 16,
                right: 16
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.contain,
                          image: AssetImage("assets/images/VirtualSecurity_1-removebg-preview.png")
                        )
                      ),
                    ),
                  ),
                  const Text("Sign in to Account.",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18
                    ),
                    textAlign: TextAlign.start,
                  ),
                  Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 2,
                              blurRadius: 5,
                              offset: const Offset(0, 2),
                            ),
                          ],
                        ),
                        margin: const EdgeInsets.only(top: 20),
                        child: TextFormField(
                          decoration: InputDecoration(
                            suffixIcon: Image.asset("assets/icons/mail.png"),
                            hintText: "Email",
                            hintStyle: const TextStyle(
                              color: Colors.grey
                            ),
                            border: InputBorder.none,
                            contentPadding: const EdgeInsets.all(16.0),
                          ),
                          style: const TextStyle(
                            fontSize: 16.0,
                            color: Colors.black87,
                          ),
                          controller: loginProviderController.emailController,
                        ),
                      ),

                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 2,
                              blurRadius: 5,
                              offset: const Offset(0, 2),
                            ),
                          ],
                        ),
                        margin: const EdgeInsets.only(top: 20),
                        child: TextFormField(
                          obscureText: !isPasswordVisible,
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                              icon: Icon(
                                isPasswordVisible ? Icons.visibility : Icons.visibility_off,
                              ),
                              onPressed: () {
                                setState(() {
                                  isPasswordVisible =! isPasswordVisible;
                                });
                              },
                            ),
                            hintText: "Password",
                            hintStyle: const TextStyle(
                              color: Colors.grey
                            ),
                            border: InputBorder.none,
                            contentPadding: const EdgeInsets.all(16.0),
                          ),
                          style: const TextStyle(
                            fontSize: 16.0,
                            color: Colors.black87,
                          ),
                          controller: loginProviderController.passwordController,
                        ),
                      ),

                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 2,
                              blurRadius: 5,
                              offset: const Offset(0, 2),
                            ),
                          ],
                        ),
                        margin: const EdgeInsets.only(top: 20),
                        child: TextFormField(
                          decoration: InputDecoration(
                            suffixIcon: Image.asset(
                              "assets/icons/badge.png",
                            ),
                            hintText: "Confirm",
                            hintStyle: const TextStyle(
                                color: Colors.grey
                            ),
                            border: InputBorder.none,
                            contentPadding: const EdgeInsets.all(16.0),
                          ),
                          style: const TextStyle(
                            fontSize: 16.0,
                            color: Colors.black87,
                          ),
                          controller: confirmPasswordController
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Checkbox(
                                fillColor:
                                    MaterialStateProperty.resolveWith((states) {
                                  if (!states
                                      .contains(MaterialState.selected)) {
                                    return Colors.transparent;
                                  }
                                  return Colors.blue;
                                }),
                                side: const BorderSide(color: Colors.red, width: 2),
                                value: _checked,
                                onChanged: (bool? value) {
                                  setState(() {
                                    _checked = value!;
                                  });
                                },
                              ),
                              Text(
                                'Remember me!',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: _checked ? Colors.green : Colors.red),
                              ),
                            ],
                          ),
                          const Row(
                            children: [
                              Text('Forget Password?', style: TextStyle(
                                decorationColor: Colors.black,
                                decoration: TextDecoration.underline
                              ),),
                            ],
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        onPressed: () async {
                          try {
                            final data = await provider.authLogin();
                            if (data == false) {
                              if (!context.mounted) return;
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  backgroundColor: Colors.transparent,
                                  elevation: 0,
                                  // Remove shadow
                                  duration: const Duration(seconds: 4),
                                  // Adjust duration as needed
                                  content: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16, vertical: 12),
                                    decoration: BoxDecoration(
                                      color: Colors.redAccent,
                                      borderRadius: BorderRadius.circular(8),
                                      boxShadow: const [
                                        BoxShadow(
                                          color: Colors.black26,
                                          blurRadius: 5.0,
                                          spreadRadius: 1.0,
                                          offset: Offset(0.0, 2.0),
                                        ),
                                      ],
                                    ),
                                    child: const Row(
                                      children: [
                                        Icon(
                                          Icons.error,
                                          color: Colors.white,
                                        ),
                                        SizedBox(width: 8),
                                        Expanded(
                                          child: Text(
                                            'Login failed. Please try again.',
                                            style: TextStyle(color: Colors.white),
                                          ),
                                        ),
                                        Icon(
                                          Icons.close,
                                          color: Colors.white,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            } else {
                              if (provider.passwordController.text ==
                                  confirmPasswordController.text) {
                                if (!context.mounted) return;
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return const Center(
                                      child: SpinKitFadingCircle(
                                        color: Colors.white,
                                        size: 35.0,
                                      )
                                    );
                                  },
                                );
                                await Future.delayed(
                                    const Duration(seconds: 1)
                                );

                                if (!context.mounted) return;
                                Navigator.of(context).pop(context);
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    backgroundColor: Colors.transparent,
                                    elevation: 0,
                                    // Remove shadow
                                    duration: const Duration(seconds: 4),
                                    // Adjust duration as needed
                                    content: Container(
                                      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                                      decoration: BoxDecoration(
                                        color: Colors.blueAccent,
                                        borderRadius: BorderRadius.circular(8),
                                        boxShadow: const [
                                          BoxShadow(
                                            color: Colors.black26,
                                            blurRadius: 5.0,
                                            spreadRadius: 1.0,
                                            offset: Offset(0.0, 2.0),
                                          ),
                                        ],
                                      ),
                                      child: const Row(
                                        children: [
                                          Icon(
                                            Icons.done,
                                            color: Colors.white,
                                          ),
                                          SizedBox(width: 8),
                                          Expanded(
                                            child: Text(
                                              'User login success.',
                                              style: TextStyle(
                                                color: Colors.white
                                              ),
                                            ),
                                          ),
                                          Icon(
                                            Icons.close,
                                            color: Colors.white,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const MainPage()));
                              } else {
                                // ignore: use_build_context_synchronously
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    backgroundColor: Colors.transparent,
                                    elevation: 0,
                                    // Remove shadow
                                    duration: const Duration(seconds: 4),
                                    // Adjust duration as needed
                                    content: Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 12),
                                      decoration: BoxDecoration(
                                        color: Colors.redAccent,
                                        borderRadius: BorderRadius.circular(8),
                                        boxShadow: const [
                                          BoxShadow(
                                            color: Colors.black26,
                                            blurRadius: 5.0,
                                            spreadRadius: 1.0,
                                            offset: Offset(0.0, 2.0),
                                          ),
                                        ],
                                      ),
                                      child: const Row(
                                        children: [
                                          Icon(
                                            Icons.error,
                                            color: Colors.white,
                                          ),
                                          SizedBox(width: 8),
                                          Expanded(
                                            child: Text(
                                              'Something wrong with confirm password!',
                                              style: TextStyle(
                                                color: Colors.white
                                              ),
                                            ),
                                          ),
                                          Icon(
                                            Icons.close,
                                            color: Colors.white,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              }
                            }
                          } catch (e) {
                            if (kDebugMode) {
                              print('Exception: $e');
                            }
                          }
                        },
                        // ...
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.indigoAccent.shade100,
                          minimumSize: const Size(double.maxFinite, 40),
                          foregroundColor: Colors.white,
                          shape: ContinuousRectangleBorder(
                            borderRadius: BorderRadius.circular(15)
                          ),
                        ),
                        child: const Text('Sign in',
                          style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)
                        ),
                      ),
                      const SizedBox(height: 30,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const Text('If you do not have an account? | '),
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                  const RegisterApp()
                                )
                              );
                            },
                            child: const Text(
                              'Register.',
                              style: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}


