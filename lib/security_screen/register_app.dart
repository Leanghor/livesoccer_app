import 'package:flutter/material.dart';
import 'package:flutter_demo3/security_screen/login_app.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import '../provider/register_provider.dart';

class RegisterApp extends StatefulWidget {
  const RegisterApp({super.key,});
  @override
  State<RegisterApp> createState() => _LoginAppState();
}

class _LoginAppState extends State<RegisterApp> {

  var name = TextEditingController(text: '');
  var email = TextEditingController(text: '');
  var password = TextEditingController(text: '');
  var confirmPassword = TextEditingController(text: '');

  RegExp regex = RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$&*~]).{8,}$');

  bool isPasswordVisible = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    name.dispose();
    email.dispose();
    password.dispose();
    confirmPassword.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.only(left: 16, right: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Container(
                  width: 200,
                  height: 200,
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/VirtualSecurity_1-removebg-preview.png")
                      )
                  ),
                ),
              ),
              const Text("Sign up to Account.",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18
                ),
                textAlign: TextAlign.start,
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: const Offset(0, 2),
                    ),
                  ],
                ),
                margin: const EdgeInsets.only(top: 20),
                child: TextFormField(
                  controller: name,
                  obscureText: false,
                  decoration: InputDecoration(
                    suffixIcon: Image.asset("assets/icons/user.png"),
                    hintText: "Username",
                    hintStyle: const TextStyle(
                        color: Colors.grey
                    ),
                    border: InputBorder.none,
                    contentPadding: const EdgeInsets.all(16.0),
                  ),
                  validator: (value){
                    if(value!.isEmpty){
                      return "Name is required";
                    }else{
                      return null;
                    }
                  },
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: const Offset(0, 2),
                    ),
                  ],
                ),
                margin: const EdgeInsets.only(top: 20),
                child: TextFormField(
                  controller: email,
                  obscureText: false,
                  decoration: InputDecoration(
                    suffixIcon: Image.asset("assets/icons/mail.png"),
                    hintText: "Email",
                    hintStyle: const TextStyle(
                        color: Colors.grey
                    ),
                    border: InputBorder.none,
                    contentPadding: const EdgeInsets.all(16.0),
                  ),
                  validator: (value){
                    if(value!.isEmpty || !RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$').hasMatch(value)){
                      return "Email is required";
                    }else{
                      return null;
                    }
                  },
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: const Offset(0, 2),
                    ),
                  ],
                ),
                margin: const EdgeInsets.only(top: 20),
                child: TextFormField(
                  controller: password,
                  obscureText: !isPasswordVisible,
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: Icon(
                        isPasswordVisible ? Icons.visibility : Icons.visibility_off,
                      ),
                      onPressed: () {
                        setState(() {
                          isPasswordVisible =! isPasswordVisible;
                        });
                      },
                    ),
                    hintText: "+ 8 character & strong",
                    hintStyle: const TextStyle(
                        color: Colors.grey
                    ),
                    border: InputBorder.none,
                    contentPadding: const EdgeInsets.all(16.0),
                  ),
                  validator:(value){RegExp regex = RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$&*~]).{8,}$');
                  if (value!.isEmpty) {
                    return 'Password is required';
                  } else {
                    if (!regex.hasMatch(value)) {
                      return 'Enter valid password';
                    } else {
                      return null;
                    }
                  }
                  },
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: const Offset(0, 2),
                    ),
                  ],
                ),
                margin: const EdgeInsets.only(top: 20),
                child: TextFormField(
                  controller: confirmPassword,
                  obscureText: false,
                  decoration: InputDecoration(
                    suffixIcon: Image.asset("assets/icons/badge.png"),
                    hintText: "Confirm",
                    hintStyle: const TextStyle(
                        color: Colors.grey
                    ),
                    border: InputBorder.none,
                    contentPadding: const EdgeInsets.all(16.0),
                  ),
                  validator: (value){
                    if(value == null || value.isEmpty){
                      return "Confirm password is required";
                    }else{
                      return null;
                    }
                  },
                ),
              ),
              const SizedBox(height: 30,),
              ElevatedButton(
                onPressed: () {
                  final FormState form = _formKey.currentState!;
                  final registerController = context.read<SignUpProvider>();
                  if (form.validate()) {
                    registerController.registerRepo(
                        username: name.text.toString(),
                        email: email.text.toString(),
                        password: password.text.toString(),
                        passwordConfirm: confirmPassword.text.toString()).then((data) async {
                      if(password.text == confirmPassword.text) {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return const Center(
                                child: SpinKitFadingCircle(
                                  color: Colors.white,
                                  size: 40.0,
                                ));
                          },
                        );
                        await Future.delayed(const Duration(seconds: 1));
                        if(!context.mounted) return;
                        Navigator.of(context).pop(context);
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> const LoginApp()));
                      }else{
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            backgroundColor: Colors.transparent,
                            elevation: 0,
                            // Remove shadow
                            duration: const Duration(seconds: 4),
                            // Adjust duration as needed
                            content: Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 16, vertical: 12),
                              decoration: BoxDecoration(
                                color: Colors.redAccent,
                                borderRadius: BorderRadius.circular(8),
                                boxShadow: const [
                                  BoxShadow(
                                    color: Colors.black26,
                                    blurRadius: 5.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(0.0, 2.0),
                                  ),
                                ],
                              ),
                              child: const Row(
                                children: [
                                  Icon(
                                    Icons.error,
                                    color: Colors.white,
                                  ),
                                  SizedBox(width: 8),
                                  Expanded(
                                    child: Text(
                                      'Password must be the same with confirm.',
                                      style: TextStyle(
                                          color: Colors.white),
                                    ),
                                  ),
                                  Icon(
                                    Icons.close,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }
                    });
                  }
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.indigoAccent.shade100,
                  minimumSize: const Size(double.maxFinite, 40),
                  foregroundColor: Colors.white,
                  shape: ContinuousRectangleBorder(
                      borderRadius: BorderRadius.circular(15)
                  ),
                ),
                child: const Text('Sign up',
                    style: TextStyle(
                        fontSize: 16, fontWeight: FontWeight.bold)
                ),
              ),
              const SizedBox(height: 10,),
              Center(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Text('Do you already have an account? | '),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const LoginApp()));
                      },
                      child: const Text('Login.', style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold
                      ),),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      )
    );
  }
}
