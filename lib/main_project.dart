import 'package:flutter/material.dart';
import 'package:flutter_demo3/provider/login_provider.dart';
import 'package:flutter_demo3/ui/match_screen.dart';
import 'package:flutter_demo3/ui/news_screen.dart';
import 'package:flutter_demo3/ui/profile_screen.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'ui/home_screen.dart';
import 'ui/league_screen.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  void initState() {
    super.initState();
    Provider.of<LoginProviderController>(context, listen: false).getUserInformation();
  }

  int currentPageIndex = 0;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  var pages = [
    const HomeScreen(),
    MatchApp(),
    const NewsScreen(),
    const LeagueApp(),
    const ProfileScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: scaffoldKey,
        body: pages.elementAt(currentPageIndex),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: currentPageIndex,
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Colors.blue,
          unselectedItemColor: Colors.grey,
          onTap: (index) {
            setState(() {
              currentPageIndex = index;
            });
          },
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.access_time),
              label: 'Match',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.newspaper_sharp),
              label: 'News',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.sports_soccer_rounded),
              label: 'League',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.menu),
              label: 'Menu',
            ),
          ],
        ),
        // bottomNavigationBar: NavigationBar(
        //   selectedIndex: currentPageIndex,
        //   onDestinationSelected: (int i) {
        //     setState(() {
        //       currentPageIndex = i;
        //     });
        //   },
        //   destinations: [
        //     const NavigationDestination(
        //       icon: Icon(CupertinoIcons.home),
        //       label: 'Home',
        //     ),
        //     NavigationDestination(
        //       icon: Image.asset(
        //         'assets/icons/football-pitch.png',
        //         width: 27,
        //         height: 27,
        //       ),
        //       label: 'Match',
        //     ),
        //     const NavigationDestination(
        //       icon: Icon(CupertinoIcons.antenna_radiowaves_left_right),
        //       label: 'News',
        //     ),
        //     NavigationDestination(
        //       icon: SvgPicture.asset(
        //         'assets/icons/football-outline.svg',
        //         width: 27,
        //         height: 27,
        //       ),
        //       label: 'League',
        //     ),
        //     const NavigationDestination(
        //       icon: Icon(CupertinoIcons.settings),
        //       label: 'Setting',
        //     ),
        //   ],
        // ),
      ),
    );
  }
}
